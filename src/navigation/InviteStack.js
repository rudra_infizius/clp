import React from 'react';
import {StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';

import {COLORS, SIZES} from '../constants';
import InviteScreen from '../screens/InviteScreen';
import WithdrawMoneyScreen from '../screens/WithdrawMoneyScreen';
import WithdrawHistoryScreen from '../screens/WithdrawHistoryScreen';
import BackButton from '../components/UI/BackButton';
import SquareIcon from '../components/UI/SquareIcon';
import History from '../../assets/images/history.svg';
import NotificationScreen from '../screens/NotificationScreen';

const InviteStack = () => {
  const navigation = useNavigation();
  const Stack = createStackNavigator();

  const HeaderRight = () => (
    <SquareIcon
      customStyle={styles.squareIconCont}
      onPress={() => navigation.navigate('withdrawHistory')}>
      <History width={22} height={20} />
    </SquareIcon>
  );

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="invite" component={InviteScreen} />
      <Stack.Screen
        name="withdraw"
        component={WithdrawMoneyScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerRight: () => <HeaderRight />,
          headerTitleAlign: 'center',
          headerTitle: 'Withdraw Money',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="withdrawHistory"
        component={WithdrawHistoryScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Withdraw History',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="notification"
        component={NotificationScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Notifications',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    // paddingVertical: SIZES.gutter - 10,
    padding: SIZES.gutter,
  },
  title: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.large,
    textAlign: 'center',
  },
  squareIconCont: {
    width: 40,
    height: 40,
    borderRadius: 10,
    backgroundColor: COLORS.lightBlueBg,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: SIZES.gutter,
  },
});

export default InviteStack;

import React from 'react';
import {StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeStack from '../navigation/HomeStack';
import RechargeStack from '../navigation/RechargeStack';
import InviteStack from '../navigation/InviteStack';
import ProfileStack from '../navigation/ProfileStack';
import {COLORS, SIZES} from '../constants';
import HomeIcon from '../../assets/images/home.svg';
import RechargeIcon from '../../assets/images/recharge.svg';
import InviteIcon from '../../assets/images/invite.svg';
import ProfileIcon from '../../assets/images/profile.svg';
import ActiveHome from '../../assets/images/home_blue.svg';
import ActiveRecharge from '../../assets/images/recharge_blue.svg';
import ActiveInvite from '../../assets/images/invite_blue.svg';
import ActiveAccount from '../../assets/images/account_blue.svg';

const BottomTabs = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {
          paddingTop: SIZES.gutter - 10,
          paddingBottom: SIZES.gutter - 15,
          height: 60,
        },
        headerShown: false,
      }}>
      <Tab.Screen
        name="homeStack"
        component={HomeStack}
        options={{
          tabBarLabel: 'Home',
          tabBarLabelPosition: 'below-icon',
          tabBarLabelStyle: styles.labelStyle,
          tabBarIcon: ({focused}) =>
            focused ? (
              <ActiveHome width={22} height={24.37} />
            ) : (
              <HomeIcon width={20} height={18} />
            ),
          tabBarHideOnKeyboard: true,
          tabBarActiveTintColor: COLORS.mainBlue,
          tabBarInactiveTintColor: COLORS.text,
        }}
      />
      <Tab.Screen
        name="rechargeStack"
        component={RechargeStack}
        options={{
          tabBarLabel: 'Recharge',
          tabBarLabelPosition: 'below-icon',
          tabBarLabelStyle: styles.labelStyle,
          tabBarIcon: ({focused}) =>
            focused ? (
              <ActiveRecharge width={22} height={24.37} />
            ) : (
              <RechargeIcon width={20} height={18} />
            ),
          tabBarHideOnKeyboard: true,
          tabBarActiveTintColor: COLORS.mainBlue,
          tabBarInactiveTintColor: COLORS.text,
        }}
      />
      <Tab.Screen
        name="inviteStack"
        component={InviteStack}
        options={{
          tabBarLabel: 'Invite',
          tabBarLabelPosition: 'below-icon',
          tabBarLabelStyle: styles.labelStyle,
          tabBarIcon: ({focused}) =>
            focused ? (
              <ActiveInvite width={30} height={24} />
            ) : (
              <InviteIcon width={19} height={21} />
            ),
          tabBarHideOnKeyboard: true,
          tabBarActiveTintColor: COLORS.mainBlue,
          tabBarInactiveTintColor: COLORS.text,
        }}
      />
      <Tab.Screen
        name="profile"
        component={ProfileStack}
        options={{
          tabBarLabel: 'Account',
          tabBarLabelPosition: 'below-icon',
          tabBarLabelStyle: styles.labelStyle,
          tabBarIcon: ({focused}) =>
            focused ? (
              <ActiveAccount width={22} height={24.37} />
            ) : (
              <ProfileIcon width={16} height={21} />
            ),
          tabBarHideOnKeyboard: true,
          tabBarActiveTintColor: COLORS.mainBlue,
          tabBarInactiveTintColor: COLORS.text,
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  labelStyle: {
    // color: COLORS.mainBlue,
    fontFamily: 'Biryani-SemiBold',
    fontSize: SIZES.tiny,
    fontWeight: '600',
    letterSpacing: 0.7,
  },
});

export default BottomTabs;

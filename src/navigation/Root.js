import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import AuthStack from './AuthStack';
import BottomTabs from './BottomTabs';
import SplashScreen from '../screens/SplashScreen';

const Root = () => {
  const {user} = useSelector(state => state.loginReducer);
  const [showSplash, setShowSplash] = useState(true);

  useEffect(() => {
    setShowSplash(false);
    return () => {
      setShowSplash(true);
    };
  }, []);

  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false, animationEnabled: false}}>
        {showSplash && <Stack.Screen name="splash" component={SplashScreen} />}
        {user.userToken == '' ? (
          <Stack.Screen name="authStack" component={AuthStack} />
        ) : (
          <Stack.Screen name="bottomTabs" component={BottomTabs} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Root;

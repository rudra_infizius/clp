import React from 'react';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {COLORS, SIZES} from '../constants';
import AccountScreen from '../screens/AccountScreen';
import WalletScreen from '../screens/WalletScreen';
import WithdrawMoneyScreen from '../screens/WithdrawMoneyScreen';
import PrfileViewScreen from '../screens/ProfileViewScreen';
import BackButton from '../components/UI/BackButton';
import AddbankScreen from '../screens/AddbankScreen';
import SquareIcon from '../components/UI/SquareIcon';
import History from '../../assets/images/history.svg';
import ViewBankDetilsScreen from '../screens/ViewBankDetilsScreen';
import AddNewBankFormScreen from '../screens/AddNewBankFormScreen';
import TermsAndConditions from '../screens/TermsAndConditions';
import PoliciesScreen from '../screens/PoliciesScreen';

const ProfileStack = () => {
  const navigation = useNavigation();
  const Stack = createStackNavigator();
  const HeaderRight = () => (
    <SquareIcon
      customStyle={styles.squareIconCont}
      onPress={() => navigation.navigate('withdrawHistory')}>
      <History width={22} height={20} />
    </SquareIcon>
  );

  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="account" component={AccountScreen} />
      <Stack.Screen
        name="wallet"
        component={WalletScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'My Wallet',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="withdraw"
        component={WithdrawMoneyScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerRight: () => <HeaderRight />,
          headerTitleAlign: 'center',
          headerTitle: 'Withdraw Money',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="addBank"
        component={AddbankScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Banks',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="profileView"
        component={PrfileViewScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Profile',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="viewBank"
        component={ViewBankDetilsScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Bank',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="addNewBankForm"
        component={AddNewBankFormScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Add New Bank',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="termsAndConditions"
        component={TermsAndConditions}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Terms and Conditions',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="privacyPolicy"
        component={PoliciesScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Privacy Policy',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    // paddingVertical: SIZES.gutter - 10,
    padding: SIZES.gutter,
  },
  title: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.large,
    textAlign: 'center',
  },
  squareIconCont: {
    width: 40,
    height: 40,
    borderRadius: 10,
    backgroundColor: COLORS.lightBlueBg,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: SIZES.gutter,
  },
});

export default ProfileStack;

import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet} from 'react-native';

import HomeScreen from '../screens/HomeScreen';
import FastParityGameScreen from '../screens/FastParityGameScreen';
import WithdrawMoneyScreen from '../screens/WithdrawMoneyScreen';
import WithdrawHistoryScreen from '../screens/WithdrawHistoryScreen';
import NotificationScreen from '../screens/NotificationScreen';
import SpareGameScreen from '../screens/SapreGameScreen';
import BconeGameScreen from '../screens/BconeGameScreen';
import EmreedGameScreen from '../screens/EmredGameScreen';
import BackButton from '../components/UI/BackButton';
import SquareIcon from '../components/UI/SquareIcon';
import History from '../../assets/images/history.svg';
import {COLORS, SIZES} from '../constants';

const HomeStack = () => {
  const navigation = useNavigation();
  const Stack = createStackNavigator();

  const HeaderRight = () => (
    <SquareIcon
      customStyle={styles.squareIconCont}
      onPress={() => navigation.navigate('withdrawHistory')}>
      <History width={22} height={20} />
    </SquareIcon>
  );
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="home" component={HomeScreen} />
      <Stack.Screen name="fastParity" component={FastParityGameScreen} />
      <Stack.Screen name="sapre" component={SpareGameScreen} />
      <Stack.Screen name="bcone" component={BconeGameScreen} />
      <Stack.Screen name="emred" component={EmreedGameScreen} />
      <Stack.Screen
        name="withdraw"
        component={WithdrawMoneyScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerRight: () => <HeaderRight />,
          headerTitleAlign: 'center',
          headerTitle: 'Withdraw Money',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="withdrawHistory"
        component={WithdrawHistoryScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Withdraw History',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
      <Stack.Screen
        name="notification"
        component={NotificationScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Notifications',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    // paddingVertical: SIZES.gutter - 10,
    padding: SIZES.gutter,
  },
  title: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.large,
    textAlign: 'center',
  },
  squareIconCont: {
    width: 40,
    height: 40,
    borderRadius: 10,
    backgroundColor: COLORS.lightBlueBg,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: SIZES.gutter,
  },
});

export default HomeStack;

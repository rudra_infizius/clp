import React from 'react';
import {StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import RechargeScreen from '../screens/RechargeScreen';
import NotificationScreen from '../screens/NotificationScreen';
import {COLORS, SIZES} from '../constants';
import BackButton from '../components/UI/BackButton';

const RechargeStack = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="recharge" component={RechargeScreen} />
      <Stack.Screen
        name="notification"
        component={NotificationScreen}
        options={{
          headerShown: true,
          headerLeft: () => <BackButton />,
          headerTitleAlign: 'center',
          headerTitle: 'Notifications',
          headerTitleStyle: styles.title,
          headerBackgroundContainerStyle: styles.container,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    // paddingVertical: SIZES.gutter - 10,
    padding: SIZES.gutter,
  },
  title: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.large,
    textAlign: 'center',
  },
});

export default RechargeStack;

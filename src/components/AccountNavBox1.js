import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Accountsinglenav from '../components/Accountsinglenav';
import Usericon from '../../assets/images/Usericon.svg';
import Bellicon from '../../assets/images/bellicon.svg';
import Walleticon from '../../assets/images/Walleticon.svg';
import Bankicon from '../../assets/images/bankicon.svg';
import Withdrawicon from '../../assets/images/Withdrawlcon.svg';
import Refericon from '../../assets/images/refericon.svg';
import Arrow from '../../assets/images/Arrow.svg';
import Togglebutton from '../components/Togglebutton';
import {COLORS, SIZES} from '../constants';

const AccountNavBox1 = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.card}>
      <Accountsinglenav
        icon={<Usericon />}
        name="Profile"
        backgroundColor="#14C25A"
        secondaryicon={<Arrow />}
        onPress={() => navigation.navigate('profileView')}
      />
      <Accountsinglenav
        icon={<Bellicon />}
        name="Notification"
        backgroundColor="#F2C113"
        secondaryicon={<Togglebutton />}
        onPress={() => {}}
      />
      <Accountsinglenav
        icon={<Walleticon />}
        name="Wallet"
        backgroundColor="#7513F2"
        secondaryicon={<Arrow />}
        onPress={() => navigation.navigate('wallet')}
      />
      <Accountsinglenav
        icon={<Bankicon />}
        name="Bank"
        backgroundColor="#F38D15"
        secondaryicon={<Arrow />}
        onPress={() => navigation.navigate('addBank')}
      />
      <Accountsinglenav
        icon={<Withdrawicon />}
        name="Withdraw"
        backgroundColor="#C343FF"
        secondaryicon={<Arrow />}
        onPress={() => navigation.navigate('withdraw')}
      />
      <Accountsinglenav
        icon={<Refericon />}
        name="Refer & Earn"
        backgroundColor="#121824"
        secondaryicon={<Arrow />}
        onPress={() => navigation.navigate('inviteStack')}
      />
    </View>
  );
};

export default AccountNavBox1;
const styles = StyleSheet.create({
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
});

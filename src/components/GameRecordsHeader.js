import React from 'react';
import {Text, View, StyleSheet, Dimensions, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';

const GameRecordsHeader = () => {
  return (
    <View style={styles.mainview}>
      <View style={styles.cardheader}>
        <Text style={styles.label}>Period</Text>
        <Text style={styles.label}>Result</Text>
        <Text style={styles.label}>Price</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainview: {
    backgroundColor: COLORS.white,
    paddingHorizontal: SIZES.gutter - 10,
  },
  cardheader: {
    flexDirection: 'row',
    backgroundColor: COLORS.white,
    paddingVertical: SIZES.gutter - 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  label: {
    color: COLORS.text,
    fontSize: SIZES.medium,
    fontFamily: 'Biryani-Bold',
    letterSpacing: 1,
  },
});
export default GameRecordsHeader;

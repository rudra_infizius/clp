import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const ColoringCircle = props => {
  return (
    <View style={styles.circle}>
      <View backgroundColor={props.color1} style={styles.color1}></View>
      <View backgroundColor={props.color2} style={styles.color2}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  circle: {
    height: 30,
    width: 30,
    borderRadius: 15,
    flexDirection: 'row',
    transform: [{rotate: '60deg'}],
    marginHorizontal: windowWidth / 14,
  },
  color1: {
    flex: 1,
    borderBottomLeftRadius: 15,
    borderTopLeftRadius: 15,
  },
  color2: {
    flex: 1,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
  },
});

export default ColoringCircle;

import React from 'react';
import {StyleSheet, TouchableHighlight, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';

const WithdrawOption = props => {
  return (
    <TouchableHighlight
      onPress={props.onPress}
      activeOpacity={0.5}
      underlayColor={COLORS.grayBg}
      style={[
        props.isActive ? styles.activeContainer : styles.container,
        props.bgColor && {backgroundColor: props.bgColor},
      ]}>
      {props.children}
    </TouchableHighlight>
  );
};

export default WithdrawOption;

const styles = StyleSheet.create({
  container: {
    paddingVertical: SIZES.gutter - 10,
    paddingHorizontal: SIZES.gutter,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeContainer: {
    paddingVertical: SIZES.gutter - 10,
    paddingHorizontal: SIZES.gutter,
    borderRadius: 8,
    borderColor: COLORS.mainBlue,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.small,
    letterSpacing: 0.7,
  },
});

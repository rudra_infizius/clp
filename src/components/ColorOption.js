import React from 'react';
import {StyleSheet, Text, TouchableHighlight, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';

const ColorOption = props => {
  return (
    <TouchableHighlight
      onPress={props.onPress}
      activeOpacity={0.5}
      underlayColor={COLORS.grayBg}
      disabled={props.disabled}
      style={[
        styles.container,
        props.bgColor && {backgroundColor: props.bgColor},
      ]}>
      <Text style={[styles.label, props.txtColor && {color: props.txtColor}]}>
        {props.label}
      </Text>
    </TouchableHighlight>
  );
};

export default ColorOption;

const styles = StyleSheet.create({
  container: {
    paddingVertical: SIZES.gutter - 10,
    paddingHorizontal: SIZES.gutter,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.small,
    letterSpacing: 0.7,
  },
});

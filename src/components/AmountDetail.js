import React from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';

import {SIZES, COLORS, FONTS} from '../constants';
import Bank from '../../assets/images/bank.svg';

const AmountDetail = props => {
  return (
    // <View style={styles.balanceCont}>
    <View style={styles.cardBody}>
      <View style={styles.iconCont}>
        <Bank width={37} height={37} />
      </View>
      <View>
        <Text style={styles.helperTxt}>Total Balance</Text>
        <Text style={styles.balanceAmnt}>Rs. {props.amount}</Text>
        <Text style={styles.helperTxt}>ID: {props.userID}</Text>
      </View>
    </View>
    // </View>
  );
};

export default AmountDetail;

const styles = StyleSheet.create({
  cardBody: {
    flexDirection: 'row',
    marginBottom: SIZES.gutter - 10,
  },
  iconCont: {
    backgroundColor: COLORS.lightBlueBg,
    padding: SIZES.gutter + 2,
    borderRadius: 10,
    marginEnd: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  balanceAmnt: {
    color: COLORS.mainBlue,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.h1,
    fontWeight: '700',
  },
});

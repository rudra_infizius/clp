import {StyleSheet, Text, TouchableOpacity, Platform} from 'react-native';
import React from 'react';
import {COLORS, SIZES, FONTS} from '../constants';

const MoneyButton = props => {
  const betAmount = props.value;
  // console.log(betAmount);
  // console.log(props.label);
  return (
    <TouchableOpacity
      style={props.label === betAmount ? styles.selectedBtn : styles.moneyBtn}
      onPress={props.onPress}>
      <Text
        style={
          props.label === betAmount
            ? [styles.helperTxt, {color: COLORS.white}]
            : styles.helperTxt
        }>
        &#x20B9; {props.label}
      </Text>
    </TouchableOpacity>
  );
};

export default MoneyButton;

const styles = StyleSheet.create({
  moneyBtn: {
    borderRadius: 7,
    borderColor: COLORS.defaultTxt,
    borderWidth: 1,
    padding: SIZES.gutter - 10,
  },
  selectedBtn: {
    backgroundColor: COLORS.black,
    borderRadius: 7,
    padding: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
});

import React from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';
import {COLORS, FONTS, SIZES} from '../constants';

const RechargeMethodCard = props => {
  return (
    <View style={styles.container}>
      <View style={styles.providerInfoCont}>
        <View style={styles.iconCont}>{props.icon}</View>
        <View>
          <Text style={styles.name}>{props.providerName}</Text>
          <Text style={styles.helperTxt}>{props.date}</Text>
        </View>
      </View>
      <View style={styles.amntDetailsCont}>
        <Text
          style={
            props.status === 'Successfully'
              ? [styles.helperTxt, {color: COLORS.green, fontSize: SIZES.body}]
              : [styles.helperTxt, {color: COLORS.red}]
          }>
          + &#x20B9;{props.amount}
        </Text>
        <Text
          style={
            props.status === 'Successfully'
              ? [styles.helperTxt, {color: COLORS.green}]
              : [styles.helperTxt, {color: COLORS.red}]
          }>
          {props.status}
        </Text>
      </View>
    </View>
  );
};

export default RechargeMethodCard;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 7,
    borderWidth: 1,
    borderColor: COLORS.grayBg,
    marginBottom: SIZES.gutter - 7,
    paddingHorizontal: SIZES.gutter - 10,
    paddingVertical: SIZES.gutter - 15,
  },
  providerInfoCont: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconCont: {
    backgroundColor: COLORS.lightBlueBg,
    alignItems: 'center',
    justifyContent: 'center',
    height: 43,
    width: 43,
    borderRadius: 7,
    marginEnd: SIZES.gutter - 10,
  },
  name: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    fontWeight: '700',
    letterSpacing: 1,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small,
    fontWeight: '400',
  },
  amntDetailsCont: {
    alignItems: 'flex-end',
  },
  amntTxt: {
    // color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small,
    fontWeight: '400',
  },
});

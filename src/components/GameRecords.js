import React from 'react';
import {View, Text, StyleSheet, Dimensions, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';
import GameRecordsHeader from './GameRecordsHeader';

const GameRecords = props => {
  let gameColor = props.color;
  let color1;
  let color2;
  switch (gameColor) {
    case 'Red':
      color1 = COLORS.red;
      color2 = COLORS.red;
      break;
    case 'Violet':
      color1 = COLORS.violet;
      color2 = COLORS.violet;
      break;
    case 'Green':
      color1 = COLORS.green;
      color2 = COLORS.green;
      break;
    case 'RedViolet':
      color1 = COLORS.red;
      color2 = COLORS.violet;
      break;
    case 'RedGreen':
      color1 = COLORS.red;
      color2 = COLORS.green;
      break;
    case 'VioletGreen':
      color1 = COLORS.violet;
      color2 = COLORS.green;
      break;
    case 'VioletRed':
      color1 = COLORS.violet;
      color2 = COLORS.red;
      break;
    case 'GreenRed':
      color1 = COLORS.green;
      color2 = COLORS.red;
      break;
    case 'GreenViolet':
      color1 = COLORS.green;
      color2 = COLORS.violet;
      break;
    default:
      color1 = COLORS.white;
      color2 = COLORS.white;
      break;
  }
  return (
    <>
      <View style={styles.mainview}>
        <View style={styles.underview}>
          <View style={styles.periodview}>
            <Text style={styles.textcolor}>{props.period}</Text>
          </View>
          <View style={styles.circle}>
            <View backgroundColor={color1} style={styles.color1}></View>
            <View backgroundColor={color2} style={styles.color2}></View>
          </View>
          <View style={styles.priceview}>
            <Text style={styles.textcolor}>{props.price}</Text>
          </View>
        </View>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  mainview: {
    backgroundColor: COLORS.white,
    marginTop: 2,
    paddingHorizontal: SIZES.gutter - 10,
  },
  underview: {
    flexDirection: 'row',
    paddingVertical: SIZES.gutter - 10,
    backgroundColor: COLORS.white,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  textcolor: {
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    color: COLORS.text,
  },
  circle: {
    height: 30,
    width: 30,
    alignSelf: 'center',
    borderRadius: 15,
    flexDirection: 'row',
    transform: [{rotate: '60deg'}],
  },
  color1: {
    flex: 1,
    borderBottomLeftRadius: 15,
    borderTopLeftRadius: 15,
  },
  color2: {
    flex: 1,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
  },
});
export default GameRecords;

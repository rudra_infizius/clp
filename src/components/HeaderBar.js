import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {COLORS, SIZES, FONTS} from '../constants';
import Notification from '../../assets/images/notification.svg';

const HeaderBar = ({name, notifications, profileImg}) => {
  const navigation = useNavigation();

  let totalNotifications = notifications;
  return (
    <View style={styles.headerCont}>
      <View>
        <Text style={styles.greeting}>Hello, Good Morning</Text>
        <Text style={styles.name}>{name}</Text>
      </View>
      <View>
        <View style={styles.notificationArea}>
          <TouchableOpacity onPress={() => navigation.navigate('notification')}>
            <Notification width={24} height={24} />
            {totalNotifications > 0 && (
              <View style={styles.notificationCountCont}>
                <Text style={styles.notificationCount}>
                  {totalNotifications}
                </Text>
              </View>
            )}
          </TouchableOpacity>
          <View style={{marginLeft: SIZES.gutter}}>
            <Image source={profileImg} />
          </View>
        </View>
      </View>
    </View>
  );
};

export default HeaderBar;

const styles = StyleSheet.create({
  headerCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: COLORS.white,
    paddingVertical: SIZES.gutter - 15,
    paddingHorizontal: SIZES.gutter,
  },
  greeting: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small,
    fontWeight: '400',
  },
  name: {
    color: COLORS.text,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.medium,
    fontWeight: '700',
    letterSpacing: 1,
  },
  notificationArea: {
    padding: SIZES.gutter - 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  notificationCountCont: {
    position: 'absolute',
    top: -5,
    right: -5,
    width: 18,
    height: 18,
    borderRadius: 9,
    backgroundColor: COLORS.mainBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  notificationCount: {
    color: COLORS.white,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.tiny,
    fontWeight: '400',
  },
});

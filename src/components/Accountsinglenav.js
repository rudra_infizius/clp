import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import {COLORS, FONTS, SIZES} from '../constants';
// import Usericon from '../images/Usericon.svg';
// import Arrow from '../images/Arrow.svg';

const Accountsinglenav = props => {
  return (
    <TouchableOpacity style={styles.mainview} onPress={props.onPress}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <View {...props} style={styles.usericonview}>
          {props.icon}
        </View>
        <View style={styles.profileview}>
          <Text style={styles.profiletext}>{props.name}</Text>
        </View>
      </View>
      <View style={styles.icons}>
        {props.secondaryicon && props.secondaryicon}
      </View>
    </TouchableOpacity>
  );
};

export default Accountsinglenav;
const styles = StyleSheet.create({
  mainview: {
    flexDirection: 'row',
    // height: 57,
    // width: '100%',
    // marginHorizontal: 17,
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#ECEDEF',
    marginBottom: SIZES.gutter - 5,
    justifyContent: 'space-between',
    paddingVertical: SIZES.gutter - 14,
  },
  usericonview: {
    // backgroundColor: '#14C25A',
    height: 35,
    width: 35,
    borderRadius: 17,
    alignItems: 'center',
    justifyContent: 'center',
    // marginVertical: 13,
    marginLeft: 18,
  },
  profileview: {
    marginLeft: SIZES.gutter - 5,
  },
  profiletext: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small,
    letterSpacing: 1.5,
  },
  icons: {
    paddingRight: SIZES.gutter,
  },
});

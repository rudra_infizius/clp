import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';
import Watch from '../../assets/images/watch.svg';

import {COLORS, SIZES, FONTS} from '../constants';

const CountDown = props => {
  return (
    <View>
      <Text style={styles.helperTxt}>Count Down</Text>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Watch width={17} height={24} />
        <Text
          style={styles.bigText}>{`${props.minutes}:${props.seconds}`}</Text>
      </View>
    </View>
  );
};

export default CountDown;

const styles = StyleSheet.create({
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  bigText: {
    color: COLORS.mainBlue,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    letterSpacing: 1,
    marginLeft: SIZES.gutter - 15,
  },
});

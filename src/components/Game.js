import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import Stopwatch from '../../assets/images/watch.svg';
import {COLORS, SIZES, images, FONTS} from '../constants';

const Game = props => {
  return (
    <View style={styles.container}>
      <View>
        {props.icon}
        <TouchableOpacity
          style={styles.btn}
          disabled={props.isDisabled}
          onPress={props.onPress}>
          <Text style={styles.btnTxt}>PLAY NOW</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.gameInfo}>{props.name}</Text>
      <View style={styles.gameTime}>
        <Stopwatch width={16} height={20} />
        <Text style={styles.timeTxt}>{props.time}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '46%',
    borderRadius: 7,
    backgroundColor: COLORS.white,
    paddingTop: SIZES.gutter,
    alignItems: 'center',
  },
  btn: {
    backgroundColor: COLORS.mainBlue,
    alignItems: 'center',
    paddingVertical: SIZES.gutter - 15,
    width: 122,
    borderRadius: 20,
    position: 'absolute',
    bottom: -12,
  },
  btnTxt: {
    color: COLORS.white,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.tiny,
    fontWeight: '400',
    letterSpacing: 1,
  },
  gameInfo: {
    paddingTop: SIZES.gutter,
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
    fontWeight: '400',
    letterSpacing: 1,
  },
  gameTime: {
    flexDirection: 'row',
    backgroundColor: COLORS.lightBlueBg,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: SIZES.gutter - 10,
  },
  timeTxt: {
    paddingLeft: SIZES.gutter - 15,
    color: COLORS.mainBlue,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
    fontWeight: '400',
    letterSpacing: 1,
  },
});

export default Game;

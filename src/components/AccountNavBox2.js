import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {userLoggedOut} from '../redux/actions';
import Toast from 'react-native-toast-message';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {COLORS, SIZES} from '../constants';
import Accountsinglenav from '../components/Accountsinglenav';
import Privacyicon from '../../assets/images/PrivacyIcon.svg';
import Termicon from '../../assets/images/termsicon.svg';
import Signouticon from '../../assets/images/SignoutIcon.svg';
import Arrow from '../../assets/images/Arrow.svg';

const AccountNavBox2 = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {user} = useSelector(state => state.loginReducer);

  const removeAsyncToken = async () => {
    try {
      await AsyncStorage.removeItem('token');
    } catch (error) {
      console.log(error);
    }
  };
  const signOutHandler = () => {
    try {
      dispatch(userLoggedOut({token: user.userToken})).then(status => {
        if (status == true) {
          Toast.show({
            type: 'success',
            text1: 'Successfully Logged Out.',
          });
        } else {
          Toast.show({
            type: 'error',
            text1: 'Something went wrong, Please try again..',
          });
        }
      });
      removeAsyncToken();
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <View style={styles.card}>
      <Accountsinglenav
        icon={<Termicon />}
        name="Terms and Conditions"
        backgroundColor="#14C25A"
        secondaryicon={<Arrow />}
        onPress={() => navigation.navigate('termsAndConditions')}
      />
      <Accountsinglenav
        icon={<Privacyicon />}
        name="Privacy Policy"
        backgroundColor="#F2C113"
        secondaryicon={<Arrow />}
        onPress={() => navigation.navigate('privacyPolicy')}
      />
      <Accountsinglenav
        icon={<Signouticon />}
        name="Sign Out"
        backgroundColor="#7513F2"
        onPress={() => signOutHandler()}
      />
    </View>
  );
};

export default AccountNavBox2;
const styles = StyleSheet.create({
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
});

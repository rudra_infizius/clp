import React from 'react';
import {View, Text, StyleSheet, Dimensions, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';
import ColoringCircle from './ColoringCircle';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Winnerrecords = props => {
  let gameColor = props.color;
  let color1;
  let color2;
  switch (gameColor) {
    case 'Red':
      color1 = COLORS.red;
      color2 = COLORS.red;
      break;
    case 'Violet':
      color1 = COLORS.violet;
      color2 = COLORS.violet;
      break;
    case 'Green':
      color1 = COLORS.green;
      color2 = COLORS.green;
      break;
    case 'RedViolet':
      color1 = COLORS.red;
      color2 = COLORS.violet;
      break;
    case 'RedGreen':
      color1 = COLORS.red;
      color2 = COLORS.green;
      break;
    case 'VioletGreen':
      color1 = COLORS.violet;
      color2 = COLORS.green;
      break;
    case 'VioletRed':
      color1 = COLORS.violet;
      color2 = COLORS.red;
      break;
    case 'GreenRed':
      color1 = COLORS.green;
      color2 = COLORS.red;
      break;
    case 'GreenViolet':
      color1 = COLORS.green;
      color2 = COLORS.violet;
      break;
    default:
      color1 = COLORS.white;
      color2 = COLORS.white;
      break;
  }

  return (
    <View style={styles.mainview}>
      <View style={styles.AmountandIcon}>
        <View style={styles.balanceview}>
          <Text style={styles.ruppeetext}>₹</Text>
          <Text style={styles.balancetext}>{props.amount}</Text>
        </View>
        {/* props =color1,color2 */}
        <ColoringCircle color1={color1} color2={color2} />
      </View>
      <View style={styles.row}>
        <Text style={styles.darktext}>
          Name: <Text style={styles.defaultText}>{props.name}</Text>
        </Text>
        <Text style={styles.darktext}>
          Period: <Text style={styles.defaultText}>{props.period}</Text>
        </Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  mainview: {
    backgroundColor: COLORS.white,
    elevation: 2,
    padding: SIZES.gutter,
    borderRadius: 10,
    marginTop: 10,
  },
  balancetext: {
    fontSize: SIZES.h1,
    color: '#ff8c00',
  },
  balanceview: {
    flexDirection: 'row',
  },
  ruppeetext: {
    fontSize: SIZES.h1,
    color: COLORS.black,
    marginRight: 5,
  },
  darktext: {
    color: COLORS.black,
    fontFamily: 'Biryani-Bold',

    fontSize: SIZES.medium,
  },
  defaultText: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',

    fontSize: SIZES.medium,
  },

  AmountandIcon: {
    flexDirection: 'row',
    marginBottom: 5,
    justifyContent: 'space-between',
  },
  circle: {
    height: 30,
    width: 30,
    alignSelf: 'center',
    borderRadius: 15,
    flexDirection: 'row',
    transform: [{rotate: '60deg'}],
  },
});
export default Winnerrecords;

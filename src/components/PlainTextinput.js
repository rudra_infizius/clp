import React from 'react';
import {TextInput, View, StyleSheet, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';

const PlainTextinput = props => {
  return (
    <View style={styles.inputview}>
      <TextInput
        {...props}
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
        name={props.name}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  inputview: {
    borderWidth: 1,
    borderColor: COLORS.grayBg,
    borderRadius: 15,
    paddingLeft: SIZES.gutter,
    fontSize: SIZES.medium,
    marginBottom: SIZES.gutter - 10,
    fontFamily: 'Biryani-Regular',
  },
});

export default PlainTextinput;

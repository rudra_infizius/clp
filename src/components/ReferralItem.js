import React from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';

import {COLORS, SIZES, FONTS} from '../constants';
import SquareIcon from './UI/SquareIcon';
const ReferralItem = props => {
  return (
    <View style={styles.container}>
      <View style={styles.infoCont}>
        <SquareIcon bgColor={props.iconBgColor}>{props.icon}</SquareIcon>
        <View style={{marginLeft: SIZES.gutter - 10}}>
          <Text
            style={
              props.customTitleStyle ? props.customTitleStyle : styles.label
            }>
            {props.label}
          </Text>
          <Text
            numberOfLines={2}
            style={
              props.customDescStyle ? props.customDescStyle : styles.amount
            }>
            {props.description}
          </Text>
        </View>
      </View>
      {/* secondary item */}
      <View style={styles.secondaryItemCont}>{props.secondaryItem}</View>
    </View>
  );
};

export default ReferralItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    padding: SIZES.gutter - 10,
    marginVertical: SIZES.gutter - 15,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  infoCont: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
  },
  amount: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.large,
  },
  secondaryItemCont: {
    marginRight: SIZES.gutter,
  },
});

import React from 'react';
import {View, Text, StyleSheet, Dimensions, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';

const Records = props => {
  return (
    <View style={styles.mainview}>
      <View style={styles.TimeAndMoneyView}>
        <View style={styles.balanceview}>
          <Text style={styles.ruppeetext}>₹</Text>
          <Text style={styles.balancetext}>{props.amount}</Text>
        </View>
        <Text style={styles.defaultText}>{props.date}</Text>
      </View>

      {/* row 1 */}
      <View style={styles.row}>
        <Text style={styles.darktext}>
          Period: <Text style={styles.defaultText}>{props.period}</Text>
        </Text>
        <Text style={styles.darktext}>
          Select: <Text style={styles.defaultText}>{props.color}</Text>
        </Text>
      </View>
      {/* row 2 */}
      <View style={styles.row}>
        <Text style={styles.darktext}>
          Status: <Text style={styles.defaultText}>{props.status}</Text>
        </Text>
        <Text style={styles.darktext}>
          Pre Pay: <Text style={styles.defaultText}>{props.prepay}</Text>
        </Text>
      </View>
      {/* row 3 */}
      <View style={styles.row}>
        <Text style={styles.darktext}>
          Result: <Text style={styles.defaultText}>{props.result}</Text>
        </Text>
        <Text style={styles.darktext}>
          Fee: <Text style={styles.defaultText}>{props.fee}</Text>
        </Text>
      </View>
      {/* row 4 */}
      <View style={styles.row}>
        <Text style={styles.darktext}>
          Win: ₹ <Text style={styles.defaultText}>{props.win}</Text>
        </Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  mainview: {
    backgroundColor: COLORS.white,
    elevation: 2,
    padding: SIZES.gutter,
    borderRadius: 10,
    marginTop: 10,
  },
  TimeAndMoneyView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    alignItems: 'center',
  },
  column1: {},
  column2: {},
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // width: windowWidth / 3,
  },
  tableview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  balancetext: {
    fontSize: SIZES.h1,
    color: '#ff8c00',
  },
  balanceview: {
    flexDirection: 'row',
  },
  ruppeetext: {
    fontSize: SIZES.h1,
    color: COLORS.text,
    marginRight: 2,
  },
  darktext: {
    color: COLORS.black,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
  },

  defaultText: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',

    fontSize: SIZES.medium,
  },
});

export default Records;

import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {COLORS, SIZES} from '../../constants';

const BackButton = () => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.goBack()}>
      <Icon name="chevron-back" size={20} color={COLORS.defaultTxt} />
    </TouchableOpacity>
  );
};

export default BackButton;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.grayBg,
    alignItems: 'center',
    justifyContent: 'center',
    padding: SIZES.gutter - 15,
    borderRadius: 6,
    width: 40,
    height: 40,
    marginLeft: 20,
  },
});

import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

const SquareIcon = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress && props.onPress}
      style={
        props.customStyle
          ? props.customStyle
          : [styles.roundIconCont, {backgroundColor: props.bgColor}]
      }>
      {props.children}
    </TouchableOpacity>
  );
};

export default SquareIcon;

const styles = StyleSheet.create({
  roundIconCont: {
    width: 58,
    height: 58,
    borderRadius: 10,
    // backgroundColor: '#7513F2',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

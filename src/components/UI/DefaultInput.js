import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Platform} from 'react-native';
import {SIZES, COLORS, FONTS} from '../../constants';

const DefaultInput = props => {
  // const [isFocused, setIsFocused] = useState(false);

  // const onToggleFocusHandler = () => {
  //   setIsFocused(!isFocused);
  //   console.log('f');
  // };

  return (
    <TouchableOpacity
      key={props.inputKey}
      style={
        props.stateValue === props.inputKey
          ? styles.focusedInputControl
          : styles.blurInputControl
      }
      onPress={props.onPress}>
      <Text style={styles.amntTxt}>&#x20B9; {props.value}</Text>
    </TouchableOpacity>
  );
};

export default DefaultInput;

const styles = StyleSheet.create({
  blurInputControl: {
    width: '60%',
    padding: SIZES.gutter - 10,
    borderWidth: 1,
    borderColor: COLORS.grayBg,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: SIZES.gutter - 10,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  focusedInputControl: {
    width: '60%',
    padding: SIZES.gutter - 10,
    backgroundColor: COLORS.white,
    borderWidth: 1.3,
    borderColor: COLORS.mainBlue,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: SIZES.gutter - 10,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  amntTxt: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
});

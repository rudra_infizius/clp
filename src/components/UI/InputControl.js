import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Platform,
} from 'react-native';

import {COLORS, SIZES, FONTS} from '../../constants';
import RoundIcon from './RoundIcon';

const InputControl = props => {
  const [isFocused, setIsFocused] = useState(false);

  const onInputFocusHandler = () => {
    setIsFocused(true);
  };
  const onInputBlurHandler = () => {
    setIsFocused(false);
  };

  const btnType = props.type;

  return (
    <View
      // * dynamic styles as per button styles
      style={
        isFocused
          ? btnType === 'square'
            ? [styles.focusedInputControl, {borderRadius: 10}]
            : styles.focusedInputControl
          : btnType === 'square'
          ? [styles.blurInputControl, {borderRadius: 10}]
          : styles.blurInputControl
      }>
      <RoundIcon bgColor={props.iconBgColor}>{props.icon}</RoundIcon>
      <TextInput
        style={styles.txtInput}
        autoCapitalize="none"
        autoComplete="off"
        autoCorrect={false}
        placeholder={props.placeholder}
        placeholderTextColor={COLORS.defaultTxt}
        keyboardType={props.keyboardType}
        onFocus={onInputFocusHandler}
        onBlur={onInputBlurHandler}
        onChangeText={text => props.onChangeTextHandler(text)}
        value={props.value}
        secureTextEntry={props.isSecure}
        maxLength={props.maxLength && props.maxLength}
        editable={props.editable && props.editable}
        caretHidden={props.caretHidden && props.caretHidden}
      />
      {props.secondaryItem && (
        <TouchableOpacity
          onPress={() => props.secondaryItemAction()}
          style={styles.secondaryItem}>
          {props.secondaryItem}
        </TouchableOpacity>
      )}
    </View>
  );
};

export default InputControl;

const styles = StyleSheet.create({
  blurInputControl: {
    padding: SIZES.gutter - 14,
    // backgroundColor: COLORS.green,
    borderWidth: 1,
    borderColor: COLORS.grayBg,
    borderRadius: 35,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: SIZES.gutter - 10,
  },
  focusedInputControl: {
    padding: SIZES.gutter - 12,
    backgroundColor: COLORS.white,
    borderWidth: 1.3,
    borderColor: COLORS.mainBlue,
    borderRadius: 35,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: SIZES.gutter - 10,
  },
  txtInput: {
    // backgroundColor: COLORS.white,
    flexGrow: 1,
    marginHorizontal: SIZES.gutter - 10,
    color: COLORS.text,
    fontFamily: 'Biryani-SemiBold',
    fontWeight: '600',
    fontSize: SIZES.medium,
  },
  secondaryItem: {
    marginRight: SIZES.gutter - 10,
  },
});

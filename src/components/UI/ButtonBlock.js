import React from 'react';
import {StyleSheet, Text, TouchableOpacity, Platform} from 'react-native';
import {COLORS, FONTS, SIZES} from '../../constants';

const ButtonBlock = props => {
  return (
    <TouchableOpacity
      style={props.customStyle ? props.customStyle : styles.btn}
      onPress={props.onPress}>
      <Text style={styles.btnTxt}>{props.label}</Text>
    </TouchableOpacity>
  );
};

export default ButtonBlock;

const styles = StyleSheet.create({
  btn: {
    backgroundColor: COLORS.mainBlue,
    padding: SIZES.gutter - 5,
    alignItems: 'center',
    borderRadius: 35,
  },
  btnTxt: {
    color: COLORS.white,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    letterSpacing: 1,
  },
});

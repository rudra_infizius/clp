import React from 'react';
import {StyleSheet, View} from 'react-native';

const RoundIcon = props => {
  return (
    <View style={[styles.roundIconCont, {backgroundColor: props.bgColor}]}>
      {props.children}
    </View>
  );
};

export default RoundIcon;

const styles = StyleSheet.create({
  roundIconCont: {
    width: 40,
    height: 40,
    borderRadius: 20,
    // backgroundColor: '#7513F2',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

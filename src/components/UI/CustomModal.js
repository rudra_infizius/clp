import React from 'react';
import {StyleSheet, Text, View, Modal, Platform} from 'react-native';
import {COLORS, SIZES, FONTS} from '../../constants';
import SuccessPay from '../../../assets/images/successPayment.svg';
import FailPay from '../../../assets/images/failPayment.svg';
import ButtonBlock from './ButtonBlock';

const CustomModal = props => {
  return (
    <Modal
      type={props.type}
      style={styles.modalCont}
      animationType="fade"
      visible={props.isVisible}
      onRequestClose={() => props.modalCloseHandler}>
      <View style={styles.modal}>
        <View style={{marginBottom: SIZES.gutter - 5}}>
          {props.type === 'success' ? (
            <SuccessPay width={53} height={50} />
          ) : (
            <FailPay width={53} height={50} />
          )}
        </View>
        <View style={{marginBottom: SIZES.gutter - 5}}>
          {props.type === 'success' ? (
            <Text style={[styles.title, {color: COLORS.green}]}>
              Successfully Paid
            </Text>
          ) : (
            <Text style={[styles.title, {color: COLORS.red}]}>
              Payment Failed
            </Text>
          )}
          {props.type === 'success' ? (
            <Text style={styles.helperTxt}>
              Your last payment of ₹ {props.amount} was successful
            </Text>
          ) : (
            <Text style={styles.helperTxt}>
              Your last payment of ₹ {props.amount} was failed
            </Text>
          )}
        </View>

        {/* footer */}
        {props.type === 'success' ? (
          <View style={styles.footer}>
            <ButtonBlock
              label="Thank you!"
              customStyle={styles.successBtn}
              onPress={props.modalCloseHandler}
            />
          </View>
        ) : (
          <View style={[styles.failFooter, styles.footer]}>
            <ButtonBlock
              label="No Thanks"
              customStyle={styles.noBtn}
              onPress={props.modalCloseHandler}
            />
            <ButtonBlock
              label="Try Again"
              customStyle={styles.TryAgainBtn}
              onPress={props.tryAgainHandler}
            />
          </View>
        )}
      </View>
    </Modal>
  );
};

export default CustomModal;

const styles = StyleSheet.create({
  modalCont: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#00000099',
    // paddingHorizontal: SIZES.gutter,
  },
  modal: {
    width: 340,
    height: 220,
    borderRadius: 10,
    backgroundColor: COLORS.grayBg,
    padding: SIZES.gutter - 5,
    top: '31%',
    left: '8%',
  },
  title: {
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.large,
    fontWeight: '700',
    letterSpacing: 0.7,
    marginBottom: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  successBtn: {
    borderRadius: 10,
    backgroundColor: COLORS.green,
    paddingVertical: SIZES.gutter - 10,
    paddingHorizontal: 'auto',
    alignItems: 'center',
  },
  noBtn: {
    borderRadius: 10,
    backgroundColor: COLORS.grayBg,
    paddingVertical: SIZES.gutter - 10,
    paddingHorizontal: SIZES.gutter,
  },
  TryAgainBtn: {
    borderRadius: 10,
    backgroundColor: COLORS.red,
    paddingVertical: SIZES.gutter - 10,
    paddingHorizontal: SIZES.gutter,
    marginLeft: SIZES.gutter - 15,
  },
  footer: {
    // justifyContent: 'flex-end',
  },
  failFooter: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    bottom: 0,
  },
});

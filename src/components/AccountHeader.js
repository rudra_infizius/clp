import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, Image, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {COLORS, images, SIZES, FONTS} from '../constants';
// import Profileimg from '../../assets/images/Profileimg.svg';
import {getWalletAmount, getInitialUserData} from '../redux/actions';

const AccountHeader = () => {
  const {user, wallet} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();

  const userToken = user.userToken;

  const initialApiCall = () => {
    dispatch(getInitialUserData({token: userToken}));
    dispatch(getWalletAmount({token: userToken}));
  };

  useEffect(() => {
    initialApiCall();
  }, []);

  return (
    <View style={styles.card}>
      <View style={styles.mainview}>
        <View style={styles.imageview}>
          <Image source={images.profile} />
        </View>
        <View style={styles.alltextview}>
          <Text style={styles.headtext}>Total Balance</Text>
          <Text style={styles.balancetext}>Rs.{wallet.walletAmount}</Text>
          <Text style={styles.idtext}>ID: {user.userId}</Text>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  wholeview: {flex: 1},
  mainview: {
    flexDirection: 'row',
    borderRadius: SIZES.gutter - 10,
    alignItems: 'center',
  },
  alltextview: {
    marginLeft: SIZES.gutter,
  },
  headtext: {
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small - 1,
    letterSpacing: 1,
    color: COLORS.defaultTxt,
  },
  balancetext: {
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.h1,
    letterSpacing: 1,
    color: COLORS.mainBlue,
  },
  idtext: {
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small - 1,
    letterSpacing: 1,
    color: COLORS.defaultTxt,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 10,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
});
export default AccountHeader;

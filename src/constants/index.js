import {COLORS, SIZES, FONTS} from './theme';
import {images} from './images';
import {PAYTM} from './paytm';

export {COLORS, SIZES, images, FONTS, PAYTM};

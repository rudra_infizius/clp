export const images = {
  logo: require('../../assets/images/logo.png'),
  backgroundImg: require('../../assets/images/backgroundImg.jpg'),
  profileSmall: require('../../assets/images/profileSmall.png'),
  slider1: require('../../assets/images/slider1.jpg'),
  slider2: require('../../assets/images/slider2.jpg'),
  slider3: require('../../assets/images/slider3.jpg'),
  home: require('../../assets/images/active_home.png'),
  recharge: require('../../assets/images/active_recharge.png'),
  invite: require('../../assets/images/active_invite.png'),
  account: require('../../assets/images/active_user.png'),
  inactive_home: require('../../assets/images/inactive_home.png'),
  profile: require('../../assets/images/profileImg.png'),
};

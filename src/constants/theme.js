import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export const COLORS = {
  text: '#071D4E',
  defaultTxt: '#566777',
  mainBlue: '#137AF1',
  green: '#27AE60',
  grayBg: '#5667771A',
  lightBlueBg: '#E7F2FE',
  white: '#ffffff',
  black: '#000000',
  red: '#EB5757',
  greenBg: '#EAF7F0',
  violetBg: '#F8F3FE',
  redBg: '#FEF5F6',
  violet: '#754EE0',
};

export const SIZES = {
  gutter: 20,
  body: 14,
  medium: 15,
  large: 17,
  small: 13,
  tiny: 11,
  headingsLabel: 20,
  h1: 25,
  width,
  height,
};

export const FONTS = {
  iosBold: 'Biryani-Bold',
  iosRegular: 'Biryani-Regular',
  iosSemiBold: 'Biryani-SemiBold',
};

import * as Action from './actionTypes';

const initialState = {
  user: {
    userName: '',
    userPhone: '',
    userPass: '',
    userToken: '',
    userId: '',
    referralCode: '',
  },
  sliderImages: [],
  wallet: {
    walletAmount: '',
    // walletId: '',
  },
  notification: [],
  rechargeHistory: [],
  withdrawHistory: [],
  game: {
    gamePeriod: null,
    isGameWon: null,
    gameBetItem: null,
    gameBetAmount: 0,
    gameVictoryAmount: 0,
    gameLossAmount: 0,
    gameRecords: [],
    userGameRecords: [],
    gameWinnerRecords: [],
  },
  bank: {
    totalBankLinked: [],
    linkedBankName: '',
    linkedBankAccountNumber: '',
    linkedBankHolderName: '',
    linkedBankAccountType: '',
    linkedBankAccountIFSC: '',
    bankId: 0,
    bankName: '',
    bankAccountHolderName: '',
    bankAccountNumber: '',
    bankAccountType: '',
    bankIFSCCode: '',
    bankInterfacePassword: '',
  },
  referAndEarn: {
    earnedAmount: '0',
    totalReferred: '',
    referralCode: '',
  },
};

// Reducers

export const loginReducer = (state = initialState, action) => {
  if (action.type === Action.USER_LOGGED_IN) {
    return {
      ...state,
      user: {
        ...state.user,
        userToken: action.payload.userToken,
        userPhone: action.payload.userPhone,
        userPass: action.payload.userPass,
      },
    };
  } else if (action.type === Action.USER_REGISTERED) {
    return {
      ...state,
    };
  } else if (action.type === Action.USER_VERIFIED) {
    return {
      ...state,
      user: {
        ...state.user,
        userName: action.payload.userName,
        userPass: action.payload.userPass,
        userPhone: action.payload.userPhone,
        referralCode: action.payload.referralCode,
      },
    };
  } else if (action.type === Action.USER_LOGGED_OUT) {
    return {
      ...state,
      user: {
        ...state.user,
        userToken: action.payload.userToken,
      },
    };
  } else if (action.type === Action.VIEW_PROFILE) {
    return {
      ...state,
      user: {
        ...state.user,
        userName: action.payload.userName,
        userId: action.payload.userId,
      },
    };
  } else if (action.type === Action.GET_WALLET_AMOUNT) {
    return {
      ...state,
      wallet: {
        walletAmount: action.payload.amount,
      },
    };
  } else if (action.type === Action.GET_SLIDER_IMAGES) {
    return {
      ...state,
      sliderImages: action.payload.images,
    };
  } else if (action.type === Action.GET_REFERRAL_DETAILS) {
    return {
      ...state,
      referAndEarn: {
        earnedAmount: action.payload.earnedAmount,
        totalReferred: action.payload.totalReferral,
        referralCode: action.payload.code,
      },
    };
  } else if (action.type === Action.GET_WITHDRAWAL_HISTORY) {
    return {
      ...state,
      withdrawHistory: action.payload.withdrawalHistory,
    };
  } else if (action.type === Action.GET_RECHARGE_HISTORY) {
    return {
      ...state,
      rechargeHistory: action.payload.rechargeHistory,
    };
  } else if (action.type === Action.GET_LINKED_BANKS) {
    return {
      ...state,
      bank: {
        ...state.bank,
        totalBankLinked: action.payload.linkedBanks,
      },
    };
  } else if (action.type === Action.GET_BANK_DETAILS) {
    return {
      ...state,
      bank: {
        ...state.bank,
        linkedBankName: action.payload.bankName,
        linkedBankAccountNumber: action.payload.accNumber,
        linkedBankHolderName: action.payload.holderName,
        linkedBankAccountType: action.payload.accType,
        linkedBankAccountIFSC: action.payload.ifsc,
      },
    };
  } else if (action.type === Action.GET_GAME_RECORDS) {
    return {
      ...state,
      // game: {
      //   ...state.game,
      //   gameRecords: action.payload.gameRecords,
      // },
    };
  } else if (action.type === Action.GET_USER_GAME_RECORDS) {
    return {
      ...state,
      game: {
        ...state.game,
        userGameRecords: action.payload.userGameRecords,
      },
    };
  } else if (action.type === Action.GET_WINNERS_RECORDS) {
    return {
      ...state,
      game: {
        ...state.game,
        gameWinnerRecords: action.payload.winnerRecords,
      },
    };
  } else if (action.type === Action.PASSWORD_CHANGED) {
    return {
      ...state,
    };
  }

  return state;
};

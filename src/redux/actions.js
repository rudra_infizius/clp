import {BASE_URL} from '../Http/config';
import * as Action from './actionTypes';
import axios from 'axios';
import Toast from 'react-native-toast-message';

import {useSelector} from 'react-redux';

export const userLoggedIn = ({emphone, pass}) => {
  const formdata = new FormData();
  formdata.append('emphone', emphone);
  formdata.append('pass', pass);
  formdata.append('submit', 'submit');

  try {
    return async dispatch => {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/login.php',
        data: formdata,
      });
      if (res.data.response.code === 200) {
        dispatch({
          type: Action.USER_LOGGED_IN,
          payload: {
            userPhone: emphone,
            userToken: res.data.response.token,
            userPass: pass,
          },
        });
        return res.data.response.code;
        // console.log(res.data);
      } else {
        return res.data.response.code;
      }
    };
  } catch (error) {
    console.log(error);
  }
};
// to send an OTP to mobile number
export const userRegistered = ({username, phone, password, referral_code}) => {
  const formdata = new FormData();
  formdata.append('uname', username);
  formdata.append('phone', phone);
  formdata.append('pass', password);
  formdata.append('refcode', referral_code);

  try {
    return async dispatch => {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/otp-sign-up.php',
        data: formdata,
      });
      if (res.data.response.code == 200) {
        dispatch({
          type: Action.USER_REGISTERED,
        });
        return res.data.response.code;
      } else if (res.data.response.code == 401) {
        // console.log(res.data.response.code);
        return res.data.response.code;
      } else {
        return res.data.response.code;
      }
    };
  } catch (error) {
    console.log(error);
  }
};

// verify otp
export const otpVerified = ({
  otp,
  username,
  phone,
  password,
  referral_code,
}) => {
  const formdata = new FormData();
  formdata.append('otphtml', otp);
  formdata.append('uname_p', username);
  formdata.append('phone_p', phone);
  formdata.append('pass_p', password);
  formdata.append('ref_p', referral_code);

  try {
    return async dispatch => {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/verify.php',
        data: formdata,
      });
      if (res.data.response.code == 200) {
        // console.log(res.data.response.status);
        dispatch({
          type: Action.USER_VERIFIED,
          payload: {
            userPhone: phone,
            userName: username,
            userPass: password,
            referralCode: referral_code,
          },
        });
        return res.data.response.code;
      } else {
        return res.data.response.code;
      }
    };
  } catch (error) {
    console.log(error);
  }
};

// logout
export const userLoggedOut = ({token}) => {
  const formdata = new FormData();
  formdata.append('token', token);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/logout.php',
        data: formdata,
      });
      //   console.log(res.data.response.user_data.username);
      if (res.data.response.msg == 'token changed') {
        // console.log(res.data.response.user_data.username);
        dispatch({
          type: Action.USER_LOGGED_OUT,
          payload: {
            userToken: '',
          },
        });
        return true;
      } else {
        return false;
      }
    } catch (error) {
      console.log(error);
    }
  };
};

// GET USER DATA FROM THE LOGIN TOKEN.
export const getInitialUserData = ({token}) => {
  const formdata = new FormData();
  formdata.append('token', token);
  //   console.log(token);
  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/my_profile.php',
        data: formdata,
      });
      //   console.log(res.data.response.user_data.username);
      if (res.data) {
        // console.log(res.data.response.user_data.username);
        dispatch({
          type: Action.VIEW_PROFILE,
          payload: {
            userName: res.data.response.user_data.username,
            userId: res.data.response.user_data.userid,
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  };
};

// get wallet amount
export const getWalletAmount = ({token}) => {
  const formdata = new FormData();
  formdata.append('token', token);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/profile.php',
        data: formdata,
      });

      if (res.data) {
        // console.log(res.data.response.available_balance);
        dispatch({
          type: Action.GET_WALLET_AMOUNT,
          payload: {
            amount: res.data.response.available_balance,
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  };
};

// get slider images
export const getSliderImages = ({token}) => {
  const formdata = new FormData();
  formdata.append('token', token);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/index.php',
        data: formdata,
      });

      if (res.data) {
        // console.log(res.data.response.image);
        dispatch({
          type: Action.GET_SLIDER_IMAGES,
          payload: {
            images: res.data.response.image,
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  };
};

// GET REFERRAL DATA FOR INVITE SCREEN.
export const getReferralData = ({token}) => {
  const formdata = new FormData();
  formdata.append('token', token);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/refer.php',
        data: formdata,
      });

      if (res.data) {
        dispatch({
          type: Action.GET_REFERRAL_DETAILS,
          payload: {
            earnedAmount: res.data.response.amount_earned_with_refrance,
            totalReferral: res.data.response.total_refrals,
            code: res.data.response.r_code,
          },
        });
        return res.data.response.r_code;
      }
    } catch (error) {
      console.log(error);
    }
  };
};

// get withdrawal history
export const getWithdrawalHistory = ({token, pageNumber}) => {
  const formdata = new FormData();
  formdata.append('token', token);
  formdata.append('page_number', pageNumber);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/record-withdraw.php',
        data: formdata,
      });

      if (res.data) {
        dispatch({
          type: Action.GET_WITHDRAWAL_HISTORY,
          payload: {
            withdrawalHistory: res.data.response.user_all_withdrawls,
          },
        });
      }
    } catch (error) {}
  };
};

// get recharge history
export const getRechargeHistory = ({token, pageNumber}) => {
  const formdata = new FormData();
  formdata.append('token', token);
  formdata.append('page_number', pageNumber);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/record-recharge.php',
        data: formdata,
      });

      if (res.data) {
        dispatch({
          type: Action.GET_RECHARGE_HISTORY,
          payload: {
            rechargeHistory: res.data.response.user_all_transactions,
          },
        });
      }
    } catch (error) {}
  };
};

// GET LINKED BANKS LIST
export const getLinkedBanks = ({token}) => {
  const formdata = new FormData();
  formdata.append('token', token);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/my_bank.php',
        data: formdata,
      });

      if (res.data) {
        dispatch({
          type: Action.GET_LINKED_BANKS,
          payload: {
            linkedBanks: res.data.response.user_all_banks,
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  };
};

// view bank details
export const getBankDetails = ({token, id}) => {
  const formdata = new FormData();
  formdata.append('token', token);
  formdata.append('id', id);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/view_bank.php',
        data: formdata,
      });

      if (res.data) {
        dispatch({
          type: Action.GET_BANK_DETAILS,
          payload: {
            bankName: res.data.response.bank_details.bank_name,
            holderName: res.data.response.bank_details.holder_name,
            accNumber: res.data.response.bank_details.acc_number,
            accType: res.data.response.bank_details.acc_type,
            ifsc: res.data.response.bank_details.ifsc,
          },
        });
      }
    } catch (error) {}
  };
};

// ADD BANK
export const addBank = ({
  bankName,
  accNumber,
  ifscCode,
  accType,
  holderName,
  password,
}) => {
  const {user, bank} = useSelector(state => state.loginReducer);
  const type = 'update';
  const userId = user.userId;
  const lastId = bank.bankId;
  const submit = 'submit';
  const token = user.userToken;

  const formdata = new FormData();
  formdata.append('type', type);
  formdata.append('userid', userId);
  formdata.append('bank_name', bankName);
  formdata.append('acc_num', accNumber);
  formdata.append('ifsc_code', ifscCode);
  formdata.append('id', ++lastId);
  formdata.append('submit', submit);
  formdata.append('token', token);
  formdata.append('acc_type', accType);
  formdata.append('acc_name', holderName);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/add-new-bank.php',
        data: formdata,
      });

      if (res.data) {
        dispatch({
          type: Action.BANK_ADDED,
          payload: {},
        });
      }
    } catch (error) {}
  };
};

//get referral code
export const getReferralCode = ({token}) => {
  const formdata = new FormData();
  formdata.append('token', token);

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/refer.php',
        data: formdata,
      });

      if (res.data) {
        dispatch({
          type: Action.GET_REFERRAL_CODE,
          payload: {
            code: res.data.response.r_code,
          },
        });
      }
    } catch (error) {}
  };
};

// change password
export const passwordChange = ({token, userId, oldPass, newPass}) => {
  const formdata = new FormData();
  console.log(oldPass);
  formdata.append('token', token);
  formdata.append('userid', userId);
  formdata.append('oldpass', oldPass);
  formdata.append('pass', newPass);
  formdata.append('submit', 'submit');
  formdata.append('type', 'new');

  return async dispatch => {
    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/change-pass.php',
        data: formdata,
      });
      // console.log(res.data.response);
      if (res.data.response.msg === 'Password Changed Successfully!') {
        dispatch({
          type: Action.PASSWORD_CHANGED,
        });
        return {code: res.data.response.code, msg: res.data.response.msg};
      } else {
        return {code: res.data.response.code, msg: res.data.response.msg};
      }
    } catch (error) {
      console.log(error);
    }
  };
};

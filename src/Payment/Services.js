import {PAYTM} from '../constants';
// import {SERVER_API_URL} from './Server_Url';

export const generateToken = async (orderId, amount) => {
  var myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  var formData = new FormData();
  formData.append('orderId', orderId);
  formData.append('amt', amount);

  var raw = {
    orderId: orderId,
    amt: amount,
  };

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(raw), //formData,
    // redirect: 'follow',
  };

  return await fetch(PAYTM.SERVER_URL, requestOptions)
    .then(response => response.json())
    .then(result => {
      console.log('result', result);
      return result?.body?.txnToken;
    })
    .catch(error => console.log('error service.....', error));
};
//'http://10.0.2.2:8080/token.php'
// https://colorprediction.infozium.com/api/token.php

import AllInOneSDKManager from 'paytm_allinone_react-native';
import {generateToken} from './Services';
import {PAYTM} from '../constants';
import Modal from '../components/UI/CustomModal';

export const payNow = async (amount, orderId) => {
  //   let orderId = orderId; // '7688677868766734532';
  let amt = parseInt(amount);
  let MID = PAYTM.MID;
  let URL_SCHEME = PAYTM.URL_SCHEME;
  let CALLBACK_URL = PAYTM.CALLBACK_URL;

  // console.log(amt, 'amtmmmmmm');

  const token = await generateToken(orderId, amt);
  var response;
  // console.log('tokeeennnnn', token);

  try {
    AllInOneSDKManager.startTransaction(
      orderId,
      MID,
      token,
      amt.toFixed(2),
      CALLBACK_URL + orderId,
      true,
      true,
      URL_SCHEME,
    )
      .then(result => {
        console.log('gateway response', result.RESPCODE);
        response = result;
        // return res;
      })
      .catch(err => {
        console.log('gateway error', err);
        return err;
      });
  } catch (error) {
    console.log('try catch error', error);
    return error;
  }
  return response;
};

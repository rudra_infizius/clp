import React from 'react';
import {StyleSheet, Text, View, FlatList, TouchableOpacity} from 'react-native';
import {COLORS, SIZES} from '../constants';

const data = [
  {key: 0, value: 0},
  {key: 1, value: 1},
  {key: 2, value: 2},
  {key: 3, value: 3},
  {key: 4, value: 4},
  {key: 5, value: 5},
  {key: 6, value: 6},
  {key: 7, value: 7},
  {key: 8, value: 8},
  {key: 9, value: 9},
];

const BetNumbers = props => {
  const handlePress = () => {
    props.onPress();
  };
  const renderBtn = item => (
    <TouchableOpacity
      style={styles.btn}
      onPress={() => props.onPress(item.value)}>
      <Text style={styles.btnTxt}>{item.value}</Text>
    </TouchableOpacity>
  );

  return (
    <FlatList
      data={data}
      keyExtractor={item => item.key}
      renderItem={item => renderBtn(item.item)}
      numColumns={5}
      scrollEnabled={false}
    />
  );
};

export default BetNumbers;

const styles = StyleSheet.create({
  btn: {
    backgroundColor: COLORS.lightBlueBg,
    margin: 10,
    paddingHorizontal: SIZES.gutter,
    paddingVertical: 7,
    borderRadius: 5,
  },
  btnTxt: {
    color: COLORS.black,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
  },
});

import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation, useRoute} from '@react-navigation/native';
import {COLORS, SIZES, FONTS} from '../constants';
import {getBankDetails} from '../redux/actions';

const ViewBankDetilsScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();
  const {bank, user} = useSelector(state => state.loginReducer);
  const {bankId, bankName} = route.params;
  const userToken = user.userToken;
  useEffect(() => {
    dispatch(getBankDetails({token: userToken, id: bankId}));
  }, []);
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <Text style={styles.label}>Bank Name</Text>
        <Text style={[styles.text, styles.textCont]}>
          {bank.linkedBankName}
        </Text>
        <Text style={styles.label}>Bank Account Number</Text>
        <Text style={[styles.text, styles.textCont]}>
          {bank.linkedBankAccountNumber}
        </Text>
        <Text style={styles.label}>Account Holder Name</Text>

        <Text style={[styles.text, styles.textCont]}>
          {bank.linkedBankHolderName}
        </Text>
        <Text style={styles.label}>IFSC Code</Text>

        <Text style={[styles.text, styles.textCont]}>
          {bank.linkedBankAccountIFSC}
        </Text>
      </View>
    </View>
  );
};

export default ViewBankDetilsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.grayBg,
    paddingHorizontal: SIZES.gutter,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
  textCont: {
    padding: SIZES.gutter - 10,
    borderColor: COLORS.mainBlue,
    borderWidth: 1,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: COLORS.lightBlueBg,
    marginBottom: SIZES.gutter - 10,
  },
  text: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
  },
  label: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small,
  },
});

import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  Platform,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';

import {COLORS, SIZES, images, FONTS} from '../constants';

const SplashScreen = () => {
  const navigation = useNavigation();
  const {user} = useSelector(state => state.loginReducer);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (user.userToken == '') {
        navigation.navigate('authStack');
      } else {
        navigation.navigate('bottomTabs');
      }
    }, 3000);

    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <ImageBackground source={images.backgroundImg} style={styles.container}>
      <View>
        <View style={styles.imgCont}>
          <Image source={images.logo} />
        </View>
        <View style={styles.headingCont}>
          <Text style={styles.heading}>Colour Prediction</Text>
          <Text style={styles.helperTxt}>
            Let's see how lucky are you with Colours.
          </Text>
        </View>
      </View>
    </ImageBackground>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: SIZES.gutter,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgCont: {
    alignItems: 'center',
    paddingTop: 10,
    marginBottom: SIZES.gutter - 5,
  },
  headingCont: {
    alignItems: 'center',
    // marginBottom: SIZES.gutter + 5,
  },
  heading: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    fontWeight: '700',
    marginBottom: SIZES.gutter - 10,
    textAlign: 'center',
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
    textAlign: 'center',
  },
});

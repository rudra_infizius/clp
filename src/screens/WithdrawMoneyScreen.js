import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, ScrollView, Platform} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

import {COLORS, FONTS, SIZES} from '../constants';
import AmountDetail from '../components/AmountDetail';
import InputControl from '../components/UI/InputControl';
import Rupee from '../../assets/images/rupee.svg';
// import Mastercard from '../../assets/images/Mastercard.svg';
// import PayPal from '../../assets/images/PayPal.svg';
import Paytm from '../../assets/images/Paytm.svg';
// import Visa from '../../assets/images/visa-logo.svg';
import Option from '../components/WithdrawOption';
import Profile from '../../assets/images/user.svg';
import Card from '../../assets/images/card_white.svg';
import ButtonBlock from '../components/UI/ButtonBlock';
import {getInitialUserData, getWalletAmount} from '../redux/actions';

const WithdrawMoneyScreen = () => {
  const [amount, setAmount] = useState('');
  const [selectedOption, setSelectedOption] = useState(mastercard);
  const [cardHolderName, setCardHolderName] = useState('');
  const [cardNumber, setCardNumber] = useState('');

  const dispatch = useDispatch();
  const {user, wallet} = useSelector(state => state.loginReducer);
  const userToken = user.userToken;
  useEffect(() => {
    dispatch(getInitialUserData({token: userToken}));
    dispatch(getWalletAmount({token: userToken}));
  }, []);

  const mastercard = 'mastercard';
  const visacard = 'visa';
  const paypal = 'paypal';

  const onAmountChangeTextHandler = text => {
    setAmount(text);
  };

  const withdrawOptionChangeHandler = option => {
    setSelectedOption(option);
  };

  const onCardHolderNameChangeTextHandler = text => {
    setCardHolderName(text);
  };

  const onCardNumberChangeTextHandler = text => {
    setCardNumber(text);
  };

  //TODO: add the submit logic here
  const withdrawMoneySubmitHandler = () => {
    console.log('form submitted');
  };

  return (
    <View style={styles.container}>
      {/* available amount details */}
      <View style={styles.card}>
        <AmountDetail amount={wallet.walletAmount} userID={user.userId} />
      </View>

      <ScrollView
        showsVerticalScrollIndicator={false}
        scrollEventThrottle={10}
        contentContainerStyle={{paddingBottom: SIZES.gutter}}>
        <Text style={styles.separatorTxt}>Amount</Text>
        {/* withdraw amount input */}
        <View style={styles.card}>
          <InputControl
            placeholder="Enter Amount"
            keyboardType="number-pad"
            onChangeTextHandler={onAmountChangeTextHandler}
            value={amount}
            iconBgColor={COLORS.green}
            icon={<Rupee width={9} height={15} />}
            isSecure={false}
            type="square"
          />
          <Text style={styles.helperTxt}>
            *Withdraw Amount should be equal to more then ₹500.
          </Text>
        </View>

        <Text style={styles.separatorTxt}>Withdraw to</Text>

        <View style={styles.card}>
          <View style={styles.optionCont}>
            {/* <Option
              bgColor={COLORS.redBg}
              onPress={() => withdrawOptionChangeHandler(mastercard)}
              isActive={selectedOption === mastercard ? true : false}>
              <Mastercard width={45} height={27} />
            </Option> */}
            <Option
              bgColor={COLORS.lightBlueBg}
              onPress={() => withdrawOptionChangeHandler(paypal)}
              isActive={selectedOption === paypal ? true : false}>
              <Paytm width={60} height={27} />
            </Option>
            {/* <Option
              bgColor={COLORS.violetBg}
              onPress={() => withdrawOptionChangeHandler(visacard)}
              isActive={selectedOption === visacard ? true : false}>
              <Visa width={45} height={27} />
            </Option> */}
          </View>

          {/* card details */}
          <View style={{marginTop: SIZES.gutter}}></View>
          <InputControl
            placeholder="Enter Card Holder Name"
            keyboardType="default"
            onChangeTextHandler={onCardHolderNameChangeTextHandler}
            value={cardHolderName}
            iconBgColor={COLORS.violet}
            icon={<Profile width={14} height={14} />}
            isSecure={false}
            type="square"
          />
          <InputControl
            placeholder="Enter Card Number"
            keyboardType="default"
            onChangeTextHandler={onCardNumberChangeTextHandler}
            value={cardNumber}
            iconBgColor="#F38D15"
            icon={<Card width={17} height={17} />}
            isSecure={false}
            type="square"
          />
        </View>

        <View style={{marginTop: SIZES.gutter - 5}}>
          <ButtonBlock label="Withdraw" onPress={withdrawMoneySubmitHandler} />
        </View>
      </ScrollView>
    </View>
  );
};

export default WithdrawMoneyScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.gutter,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
  separatorTxt: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    fontWeight: '700',
    letterSpacing: 1,
    marginTop: SIZES.gutter,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small,
    fontWeight: '400',
  },
  optionCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

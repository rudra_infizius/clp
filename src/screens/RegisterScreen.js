import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import Toast from 'react-native-toast-message';

import {FONTS, COLORS, SIZES, images} from '../constants';
import Phone from '../../assets/images/phone.svg';
import Key from '../../assets/images/key.svg';
import User from '../../assets/images/user.svg';
import Referral from '../../assets/images/referal.svg';
import EyeOpen from '../../assets/images/eyeOpen.svg';
import EyeClose from '../../assets/images/eyeClose.svg';
import InputControl from '../components/UI/InputControl';
import ButtonBlock from '../components/UI/ButtonBlock';
import {userRegistered} from '../redux/actions';

const RegisterScreen = () => {
  const navigation = useNavigation();
  const [username, setUsername] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [confPassword, setConfPassword] = useState('');
  const [refCode, setRefCode] = useState('');
  const [isPassSecure, setIsPassSecure] = useState(true);
  const [isConfPassSecure, setIsConfPassSecure] = useState(true);

  const dispatch = useDispatch();

  const onUsernameChangeTextHandler = text => {
    setUsername(text);
  };

  const onPhoneChangeTextHandler = text => {
    setPhone(text);
  };

  const onPasswordChangeTextHandler = text => {
    setPassword(text);
  };

  const onConfPasswordChangeTextHandler = text => {
    setConfPassword(text);
  };

  const registerHandler = () => {
    const usernameRegex = /^(?=.{4,20}$)(?![_.-])[a-zA-Z0-9_-\s]+([^._-])$/;
    const phoneRegex = /^([+]?\d{1,2}[-\s]?|)\d{3}[-\s]?\d{3}[-\s]?\d{4}$/;
    const passRegex = /^((?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$/;

    if (
      usernameRegex.test(username) === true &&
      phoneRegex.test(phone) === true &&
      passRegex.test(password) === true &&
      password === confPassword
    ) {
      // setIsFormValid(true);

      let res = actionTrigger().then(res => {
        if (res == 401) {
          Toast.show({
            type: 'error',
            text1: 'Already registered',
            text2: 'Mobile number is already registered, Please Login.',
          });
          navigation.navigate('login');
        } else if (res == 200) {
          Toast.show({
            type: 'success',
            text1: 'Otp sent successfully',
            text2: 'Please verify the OTP sent on your given number!',
          });
          navigation.navigate('otp', {
            username: username,
            userPhone: phone,
            userPass: password,
            refCode: refCode,
          });
        }
      });
    } else {
      // setIsFormValid(false);
      Toast.show({
        type: 'error',
        text1: 'Unable to Register!!',
        text2: 'Please check the inputs again.',
      });
    }
    // console.log('form submitted.');
  };

  const actionTrigger = () => {
    const result = dispatch(
      userRegistered({
        username: username,
        phone: phone,
        password: password,
        referral_code: refCode,
      }),
    );
    return result;
  };

  return (
    <ImageBackground source={images.backgroundImg} style={styles.container}>
      {/* logo container */}
      <View style={styles.imgCont}>
        <Image source={images.logo} />
      </View>

      {/* screen heading container */}
      <View style={styles.headingCont}>
        <Text style={styles.heading}>Register Now</Text>
        <Text style={styles.helperTxt}>
          Please enter the details below to continue.
        </Text>
      </View>

      {/* login form container */}
      <ScrollView>
        <InputControl
          placeholder="Enter Username"
          keyboardType="default"
          onChangeTextHandler={onUsernameChangeTextHandler}
          value={username}
          iconBgColor={'#14C25A'}
          icon={<User width={15} height={15} />}
          isSecure={false}
        />
        <InputControl
          placeholder="Enter Phone number"
          keyboardType="number-pad"
          onChangeTextHandler={onPhoneChangeTextHandler}
          value={phone}
          iconBgColor={'#7513F2'}
          icon={<Phone width={15} height={15} />}
          isSecure={false}
        />
        <InputControl
          placeholder="Enter Password"
          keyboardType="default"
          onChangeTextHandler={onPasswordChangeTextHandler}
          value={password}
          iconBgColor={'#F38D15'}
          icon={<Key width={15} height={15} />}
          isSecure={isPassSecure}
          secondaryItem={
            isPassSecure ? (
              <EyeClose width={20} height={20} />
            ) : (
              <EyeOpen width={20} height={20} />
            )
          }
          secondaryItemAction={() => setIsPassSecure(!isPassSecure)}
        />
        <InputControl
          placeholder="Confirm Password"
          keyboardType="default"
          onChangeTextHandler={onConfPasswordChangeTextHandler}
          value={confPassword}
          iconBgColor={'#F38D15'}
          icon={<Key width={15} height={15} />}
          isSecure={isConfPassSecure}
          secondaryItem={
            isConfPassSecure ? (
              <EyeClose width={20} height={20} />
            ) : (
              <EyeOpen width={20} height={20} />
            )
          }
          secondaryItemAction={() => setIsConfPassSecure(!isConfPassSecure)}
        />
        <InputControl
          placeholder="Enter Referral Code (Optional)"
          keyboardType="default"
          onChangeTextHandler={text => setRefCode(text)}
          value={refCode}
          iconBgColor={'#C343FF'}
          icon={<Referral width={15} height={15} />}
          isSecure={false}
        />

        {/* login btn */}
        <ButtonBlock label="Register" onPress={registerHandler} />

        {/* register button */}
      </ScrollView>
      <View style={styles.loginBtnCont}>
        <Text style={styles.helperTxt}>Already have an account?</Text>
        <TouchableOpacity onPress={() => navigation.navigate('login')}>
          <Text
            style={[
              styles.helperTxt,
              {fontSize: SIZES.body, color: COLORS.mainBlue},
            ]}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: SIZES.gutter,
  },
  imgCont: {
    alignItems: 'center',
    paddingTop: 10,
    marginBottom: SIZES.gutter - 5,
    marginTop: Platform.OS === 'ios' ? 30 : null,
  },
  headingCont: {
    alignItems: 'center',
    marginBottom: SIZES.gutter + 5,
  },
  heading: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    fontWeight: '700',
    marginBottom: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  loginBtnCont: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
  },
});

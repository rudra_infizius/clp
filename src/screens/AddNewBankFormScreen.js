import React, {useState} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {COLORS, SIZES} from '../constants';
import PlainTextinput from '../components/PlainTextinput';
import ButtonBlock from '../components/UI/ButtonBlock';

const AddNewBankFormScreen = () => {
  const [state, setstate] = useState({
    bankname: '',
    accountholder: '',
    accountnumber: '',
    accounttype: '',
    bankifsc: '',
    paypassword: '',
  });

  function handleChange(evt) {
    const value = evt.target.value;
    setstate({
      ...state,
      [evt.target.name]: value,
    });
  }
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.card}>
        <PlainTextinput
          placeholder="Enter Bank Name"
          name="bankname"
          defaultValue={state.bankname}
          onChange={handleChange}
        />
        <PlainTextinput
          placeholder="Enter Account Holder Name"
          name="accountholder"
          defaultValue={state.accountholder}
          onChange={handleChange}
        />
        <PlainTextinput
          placeholder="Enter Bank Account Number"
          name="accountnumber"
          defaultValue={state.accountnumber}
          onChange={handleChange}
        />
        <PlainTextinput
          placeholder="Enter Account Type"
          name="accounttype"
          defaultValue={state.accounttype}
          onChange={handleChange}
        />
        <PlainTextinput
          placeholder="Enter Bank IFSC Code"
          name="bankifsc"
          defaultValue={state.bankifsc}
          onChange={handleChange}
        />
        <PlainTextinput
          placeholder="Enter Pay Password"
          name="paypassword"
          defaultValue={state.paypassword}
          onChange={handleChange}
        />
      </View>
      <View style={{marginTop: SIZES.gutter}}>
        <ButtonBlock label="Add" onPress={() => {}} />
      </View>
    </ScrollView>
  );
};

export default AddNewBankFormScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.gutter,
    backgroundColor: COLORS.grayBg,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
});

import React, {useEffect} from 'react';
import {StyleSheet, View, Text, ScrollView, Platform} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import AmountDetail from '../components/AmountDetail';
import ButtonBlock from '../components/UI/ButtonBlock';
import RechargeMethodCard from '../components/RechargeMethodCard';
import Paytm from '../../assets/images/Paytm.svg';
import {COLORS, FONTS, SIZES} from '../constants';
import {
  getWalletAmount,
  getInitialUserData,
  getRechargeHistory,
} from '../redux/actions';

const WalletScreen = () => {
  const navigation = useNavigation();
  const {user, wallet, rechargeHistory} = useSelector(
    state => state.loginReducer,
  );
  const dispatch = useDispatch();

  const userToken = user.userToken;

  const initialApiCall = () => {
    dispatch(getInitialUserData({token: userToken}));
    dispatch(getWalletAmount({token: userToken}));
    dispatch(getRechargeHistory({token: userToken, pageNumber: '1'}));
  };

  useEffect(() => {
    initialApiCall();
  }, []);

  // converting status from numbers to correct props
  const status = '';
  const statusHandler = status => {
    if (status === '1') {
      return (status = 'Pending');
    } else if (status === '2') {
      return (status = 'Successfully');
    } else {
      return (status = 'Filed');
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.balanceCont}>
        <AmountDetail amount={wallet.walletAmount} userID={user.userId} />
        {/* card footer with two buttons */}
        <View
          style={[
            styles.cardBody,
            {
              justifyContent: 'space-evenly',
              marginBottom: 0,
              marginTop: SIZES.gutter - 10,
            },
          ]}>
          <ButtonBlock
            label="Recharge"
            onPress={() => navigation.navigate('rechargeStack')}
            customStyle={styles.customBtnStyle}
          />
          <View style={{paddingHorizontal: SIZES.gutter - 15}} />
          <ButtonBlock
            label="Withdrawal"
            onPress={() => navigation.navigate('withdraw')}
            customStyle={[
              styles.customBtnStyle,
              {backgroundColor: COLORS.green},
            ]}
          />
        </View>
      </View>
      {/* separator text */}
      <View>
        <Text style={styles.separatorTxt}>Recharge History</Text>
      </View>
      {/* list of transaction */}
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.card}>
          {rechargeHistory.length > 0 ? (
            rechargeHistory.map(item => (
              <RechargeMethodCard
                key={rechargeHistory[item]}
                providerName="Paytm"
                date={item.Time}
                amount={item.Amount}
                status={statusHandler(item.Status)}
                icon={<Paytm width={33} height={45} />}
              />
            ))
          ) : (
            <Text style={styles.helperTxt}>No recharges found.</Text>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

export default WalletScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.gutter,
    backgroundColor: COLORS.grayBg,
  },
  balanceCont: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    marginBottom: SIZES.gutter - 10,
  },
  cardBody: {
    flexDirection: 'row',
    marginBottom: SIZES.gutter - 10,
  },
  customBtnStyle: {
    backgroundColor: COLORS.mainBlue,
    paddingVertical: SIZES.gutter - 10,
    width: '50%',
    alignItems: 'center',
    borderRadius: 6,
  },
  separatorTxt: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    fontWeight: '700',
    letterSpacing: 1,
    marginTop: SIZES.gutter - 10,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
  helperTxt: {
    fontFamily: 'Biryani-Regular',
    color: COLORS.defaultTxt,
    textAlign: 'center',
  },
});

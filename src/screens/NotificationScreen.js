import React from 'react';
import {Text, View, StyleSheet, Platform} from 'react-native';
import Notification from '../../assets/images/NotificationBlue.svg';
import {COLORS, SIZES, FONTS} from '../constants';

const Notifications = () => {
  return (
    <View style={[styles.card, {marginHorizontal: SIZES.gutter}]}>
      <View style={styles.subview}>
        <View style={styles.iconview}>
          <Notification />
        </View>
        <View>
          <Text style={styles.headtext}>Hi There!</Text>
          <Text style={styles.maintext}>
            Introducing our latest features - inbox {'\n'}
            for all your important updates. stay {'\n'}tuned!
          </Text>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  subview: {
    flexDirection: 'row',
  },
  iconview: {
    backgroundColor: COLORS.lightBlueBg,
    height: 55,
    width: 55,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: SIZES.gutter - 5,
    borderRadius: 10,
  },
  headtext: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    letterSpacing: 1,
  },
  maintext: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.small - 1,
    letterSpacing: 0.7,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
});
export default Notifications;

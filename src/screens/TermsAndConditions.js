import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import {SIZES, COLORS, FONTS} from '../constants';
import {BASE_URL} from '../Http/config';

const TermsAndConditions = () => {
  const {user} = useSelector(state => state.loginReducer);
  const [terms, setTerms] = useState('');
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    getTermsAndCond({token: user.userToken});
  }, []);

  const getTermsAndCond = async ({token}) => {
    const formdata = new FormData();
    formdata.append('token', token);

    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/terms-and-conditions.php',
        data: formdata,
      });
      const termsAndCondition = await res.data.response.term_conditions;
      setTerms(termsAndCondition);
      setIsReady(true);
    } catch (error) {
      console.log(error);
    }
  };

  if (!isReady) {
    return (
      <ActivityIndicator
        color={COLORS.mainBlue}
        size={'large'}
        style={styles.indicatorCont}
      />
    );
  }
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.text}>{terms}</Text>
    </ScrollView>
  );
};

export default TermsAndConditions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: SIZES.gutter,
  },
  text: {
    fontFamily: 'Biryani-Regular',
    color: COLORS.defaultTxt,
    fontSize: SIZES.body,
  },
  indicatorCont: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

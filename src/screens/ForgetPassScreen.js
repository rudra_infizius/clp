import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';
import OTPInput from 'react-native-confirmation-code-input';
import {useDispatch, useSelector} from 'react-redux';
import Toast from 'react-native-toast-message';
import Icon from 'react-native-vector-icons/Ionicons';

import {COLORS, SIZES, images, FONTS} from '../constants';
import Phone from '../../assets/images/phone.svg';
import InputControl from '../components/UI/InputControl';
import ButtonBlock from '../components/UI/ButtonBlock';
import Key from '../../assets/images/key.svg';
import EyeOpen from '../../assets/images/eyeOpen.svg';
import EyeClose from '../../assets/images/eyeClose.svg';
import {BASE_URL} from '../Http/config';

const ForgetPassScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [phone, setPhone] = useState('');
  const [showOtp, setShowOtp] = useState(false);
  const [otp, setOtp] = useState('');
  const [password, setPassword] = useState('');
  const [secureEntry, setSecureEntry] = useState(true);

  const onPhoneChangeTextHandler = text => {
    setPhone(text);
  };

  const onPasswordChangeTextHandler = text => {
    setPassword(text);
  };

  const toggleSecureEntryHandler = () => {
    // console.log('pressed');
    setSecureEntry(!secureEntry);
  };

  const sendOtpHandler = async ({phone}) => {
    console.log(phone);
    const formdata = new FormData();
    formdata.append('phone', phone);
    formdata.append('submit', 'submit');

    const res = await axios({
      method: 'POST',
      data: formdata,
      url: BASE_URL + '/reset-password.php',
    });

    if (res.data.response.msg === 'Otp sent on your given number!') {
      setShowOtp(true);
    }
  };

  const verifyOtpHandler = async ({phone, otp, password}) => {
    const formdata = new FormData();
    formdata.append('phone', phone);
    formdata.append('otp', otp);
    formdata.append('pass', password);
    formdata.append('submit', 'submit');

    const res = await axios({
      method: 'POST',
      url: BASE_URL + '/reset-verify.php',
      data: formdata,
    });

    if (
      res.data.response.msg ===
      'Password Changed Successfully. Please Login to continue.'
    ) {
      Toast.show({
        type: 'success',
        text1: res.data.response.msg,
      });
      navigation.navigate('login');
    } else {
      Toast.show({
        type: 'error',
        text1: 'Something went wrong, please try again.',
      });
    }
  };

  return (
    <ImageBackground source={images.backgroundImg} style={styles.container}>
      {/* logo container */}
      <View style={styles.imgCont}>
        <Icon name="lock-closed" size={80} color={COLORS.black} />
      </View>

      {/* screen heading container */}
      <View style={styles.headingCont}>
        <Text style={styles.heading}>Forget Password?</Text>
        {showOtp ? (
          <Text>Please enter OTP to continue.</Text>
        ) : (
          <Text style={styles.helperTxt}>Please enter mobile to continue.</Text>
        )}
      </View>

      {/* login form container */}
      <ScrollView>
        {!showOtp && (
          <View>
            <InputControl
              placeholder="Enter Phone number"
              keyboardType="phone-pad"
              onChangeTextHandler={onPhoneChangeTextHandler}
              value={phone}
              iconBgColor={'#7513F2'}
              icon={<Phone width={15} height={15} />}
              isSecure={false}
              maxLength={10}
            />
            <ButtonBlock
              label="Sent OTP"
              onPress={() => sendOtpHandler({phone: phone})}
            />
          </View>
        )}

        {showOtp && (
          <View>
            <OTPInput
              keyboardType="numeric"
              codeLength={6}
              className="border-box"
              activeColor={COLORS.mainBlue}
              inactiveColor={COLORS.defaultTxt}
              size={42}
              space={14}
              codeInputStyle={styles.codeInput}
              containerStyle={styles.codeInputCont}
              value={otp}
              onFulfill={text => setOtp(text)}
            />
            <View style={styles.separator} />
            <InputControl
              placeholder="Enter Password"
              keyboardType="default"
              onChangeTextHandler={onPasswordChangeTextHandler}
              value={password}
              iconBgColor={'#F38D15'}
              icon={<Key width={15} height={15} />}
              isSecure={secureEntry}
              secondaryItem={
                secureEntry ? (
                  <EyeClose width={17} height={17} />
                ) : (
                  <EyeOpen width={17} height={17} />
                )
              }
              secondaryItemAction={toggleSecureEntryHandler}
            />
            <ButtonBlock
              label="Update"
              onPress={() =>
                verifyOtpHandler({phone: phone, otp: otp, password: password})
              }
            />
          </View>
        )}
      </ScrollView>
    </ImageBackground>
  );
};

export default ForgetPassScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: SIZES.gutter,
  },
  imgCont: {
    alignItems: 'center',
    paddingTop: 10,
    marginBottom: SIZES.gutter - 5,
    marginTop: Platform.OS === 'ios' ? 30 : null,
  },
  headingCont: {
    alignItems: 'center',
    marginBottom: SIZES.gutter + 5,
  },
  heading: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    fontWeight: '700',
    marginBottom: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  frgtPassCont: {
    flexDirection: 'row-reverse',
    marginVertical: SIZES.gutter - 10,
  },
  registerBtnCont: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
  },
  codeInput: {
    borderRadius: 10,
    fontSize: SIZES.large,
    fontWeight: '700',
    fontFamily: 'Biryani-Bold',
  },
  codeInputCont: {
    padding: 0,
    margin: 0,
    // height: 300,
  },
  separator: {
    paddingVertical: SIZES.gutter - 10,
  },
});

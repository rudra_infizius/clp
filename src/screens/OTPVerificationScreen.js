import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Platform,
} from 'react-native';
import OTPInput from 'react-native-confirmation-code-input';
import {useDispatch} from 'react-redux';
import {useNavigation, useRoute} from '@react-navigation/native';
import Toast from 'react-native-toast-message';

import {COLORS, SIZES, images, FONTS} from '../constants';
import ButtonBlock from '../components/UI/ButtonBlock';
import {otpVerified} from '../redux/actions';

const OTPVerificationScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();
  const [otp, setOtp] = useState('');

  const verifyOtpHandler = () => {
    const username = route.params.username;
    const password = route.params.userPass;
    const phone = route.params.userPhone;
    const refCode = route.params.refCode;
    // console.log(otp);
    dispatch(
      otpVerified({
        otp: otp,
        username: username,
        phone: phone,
        password: password,
        referral_code: refCode,
      }),
    ).then(code => {
      if (code == 200) {
        Toast.show({
          type: 'success',
          text1: 'Register Successful',
          text2: 'Now you can login.',
        });
        navigation.navigate('login');
      } else {
        Toast.show({
          type: 'error',
          text1: 'Something went wrong!',
          text2: 'Please try again after sometime',
        });
      }
    });
  };

  return (
    <ImageBackground source={images.backgroundImg} style={styles.container}>
      {/* logo container */}
      <View style={styles.imgCont}>
        <Image source={images.logo} />
      </View>

      {/* screen heading container */}
      <View style={styles.headingCont}>
        <Text style={styles.heading}>OTP Verification</Text>
        <Text style={styles.helperTxt}>
          Enter the OTP sent to your mobile number.
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate('register')}>
          <Text
            style={[
              styles.helperTxt,
              {fontSize: SIZES.body, color: COLORS.mainBlue, fontWeight: '600'},
            ]}>
            Change Number
          </Text>
        </TouchableOpacity>
      </View>

      {/* otp input */}
      <OTPInput
        keyboardType="numeric"
        codeLength={6}
        className="border-box"
        activeColor={COLORS.mainBlue}
        inactiveColor={COLORS.defaultTxt}
        size={42}
        space={14}
        codeInputStyle={styles.codeInput}
        containerStyle={styles.codeInputCont}
        value={otp}
        // onChangeText={text => setOtp(text)}
        onFulfill={text => setOtp(text)}
      />

      {/* verify btn */}
      <ButtonBlock label="Verify" onPress={verifyOtpHandler} />

      {/* resend otp timer */}
      <View style={styles.timerCont}>
        <Text
          style={[
            styles.helperTxt,
            {
              fontSize: SIZES.large - 1,
              color: COLORS.mainBlue,
              fontWeight: '700',
              letterSpacing: 1,
            },
          ]}>
          01:02
        </Text>
      </View>

      {/* resend otp btn */}
      <View style={styles.resendBtnCont}>
        <Text style={styles.helperTxt}>Did’t receive the OTP?</Text>
        <TouchableOpacity onPress={() => {}}>
          <Text
            style={[
              styles.helperTxt,
              {fontSize: SIZES.body, color: COLORS.mainBlue},
            ]}>
            Resend
          </Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default OTPVerificationScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: SIZES.gutter,
  },
  imgCont: {
    alignItems: 'center',
    paddingTop: '25%',
    marginBottom: SIZES.gutter - 5,
    marginTop: Platform.OS === 'ios' ? 30 : null,
  },
  headingCont: {
    alignItems: 'center',
    marginBottom: SIZES.gutter + 5,
  },
  heading: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    fontWeight: '700',
    marginBottom: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  codeInput: {
    borderRadius: 10,
    fontSize: SIZES.large,
    fontWeight: '700',
    fontFamily: 'Biryani-Bold',
  },
  codeInputCont: {
    padding: 0,
    margin: 0,
    height: 300,
  },
  timerCont: {
    padding: SIZES.gutter,
    alignItems: 'center',
  },
  resendBtnCont: {
    padding: SIZES.gutter,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    // position: 'absolute',
    // bottom: 20,
  },
});

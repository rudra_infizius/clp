import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import {COLORS, SIZES, FONTS} from '../constants';
import {BASE_URL} from '../Http/config';

const PoliciesScreen = () => {
  const {user} = useSelector(state => state.loginReducer);
  const [policies, setPolicies] = useState('');
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    getTermsAndCond({token: user.userToken});
  }, []);

  const getTermsAndCond = async ({token}) => {
    const formdata = new FormData();
    formdata.append('token', token);

    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/privacy-policy.php',
        data: formdata,
      });
      const privacyAndPolicies = await res.data.response.policies;
      const element = convertToRNText({response: privacyAndPolicies});
      setPolicies(element);
      setIsReady(true);
    } catch (error) {
      console.log(error);
    }
  };

  const convertToRNText = ({response}) => {
    var text = response.slice(59, -11);
    return text;
  };
  if (!isReady) {
    return (
      <ActivityIndicator
        color={COLORS.mainBlue}
        size={'large'}
        style={styles.indicatorCont}
      />
    );
  }
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.text}>{policies}</Text>
    </ScrollView>
  );
};

export default PoliciesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: SIZES.gutter,
  },
  text: {
    fontFamily: 'Biryani-Regular',
    color: COLORS.defaultTxt,
    fontSize: SIZES.body,
  },
  indicatorCont: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

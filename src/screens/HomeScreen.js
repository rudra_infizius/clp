import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  Platform,
  SafeAreaView,
} from 'react-native';
import Slider from 'react-native-image-slider';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {useNetInfo} from '@react-native-community/netinfo';
import Toast from 'react-native-toast-message';

import {
  getInitialUserData,
  getWalletAmount,
  getSliderImages,
} from '../redux/actions';
import {COLORS, SIZES, images, FONTS} from '../constants';
import HeaderBar from '../components/HeaderBar';
import AmountDetail from '../components/AmountDetail';
import ButtonBlock from '../components/UI/ButtonBlock';
import Game from '../components/Game';
import {BASE_URL} from '../Http/config';
import GameIcon from '../../assets/images/game.svg';

const HomeScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {user, wallet, sliderImages} = useSelector(state => state.loginReducer);

  // hook and ref for netinfo
  const NetInfo = useNetInfo();
  const internetRef = useRef(NetInfo.isConnected);

  const userToken = user.userToken;
  // console.log(userToken);
  // images from url
  const imagesForSlider = sliderImages.map(item => BASE_URL + item);
  // console.log(sliderImages);

  let totalNotifications = 0;

  const initialApiCall = () => {
    dispatch(getInitialUserData({token: userToken}));
    dispatch(getWalletAmount({token: userToken}));
    dispatch(getSliderImages({token: userToken}));
  };

  useEffect(() => {
    initialApiCall();
  }, []);

  // effect for change in internet connection
  useEffect(() => {
    internetRef.current = NetInfo.isConnected;
    if (internetRef.current) {
      initialApiCall();
      // Toast.show({
      //   type: 'success',
      //   text1: 'Connected to internet.',
      // });
    }
  }, [NetInfo.isConnected]);

  // check for internet connection.
  if (!internetRef.current) {
    Toast.show({
      type: 'error',
      text1: 'No internet connection.',
      text2: 'Turn on wifi or mobile data.',
    });
    return (
      <ActivityIndicator
        size={'large'}
        color={COLORS.mainBlue}
        style={styles.indicatorContainer}
      />
    );
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* header content */}
      <HeaderBar
        name={user.userName}
        notifications={totalNotifications}
        profileImg={images.profileSmall}
      />
      {/* <View style={{flex: 1}}>*/}

      {/* body content */}
      <ScrollView
        style={styles.container}
        showsVerticalScrollIndicator={false}
        scrollEventThrottle={16}
        contentContainerStyle={{paddingBottom: SIZES.gutter - 10}}>
        {/* image slider */}
        <View style={styles.sliderCont}>
          <Slider
            images={imagesForSlider}
            autoPlayWithInterval={3000}
            circleLoop
            autoplay
            resizeMode="contain"
            dotColor={COLORS.white}
            inactiveDotColor={COLORS.defaultTxt}
            ImageLoader={
              <ActivityIndicator size={'small'} color={COLORS.mainBlue} />
            }
          />
        </View>
        {/* //TODO: add marquee text here, designed farhan */}
        {/* balance detail card */}
        <View style={styles.balanceCont}>
          <AmountDetail amount={wallet.walletAmount} userID={user.userId} />
          {/* card footer with two buttons */}
          <View
            style={[
              styles.cardBody,
              {
                justifyContent: 'space-evenly',
                marginBottom: 0,
                marginTop: SIZES.gutter - 10,
              },
            ]}>
            <ButtonBlock
              label="Recharge"
              onPress={() => navigation.navigate('rechargeStack')}
              customStyle={styles.customBtnStyle}
            />
            <View style={{paddingHorizontal: SIZES.gutter - 15}} />
            <ButtonBlock
              label="Withdrawal"
              onPress={() => navigation.navigate('withdraw')}
              customStyle={[
                styles.customBtnStyle,
                {backgroundColor: COLORS.green},
              ]}
            />
          </View>
        </View>
        <View>
          <Text style={styles.separatorTxt}>Recent Game</Text>
        </View>
        {/* recent games content */}
        <View style={styles.gameCont}>
          <Game
            name="Parity"
            time="03 min"
            onPress={() => navigation.navigate('fastParity')}
            icon={<GameIcon width={122} height={70} />}
          />
          <View style={styles.separatorView} />
          <Game
            name="Fast Parity"
            time="N/A"
            onPress={() => navigation.navigate('sapre')}
            icon={<GameIcon width={122} height={70} />}
          />
        </View>
        <View style={[styles.gameCont, {marginTop: SIZES.gutter}]}>
          <Game
            name="Bcone"
            time="N/A"
            onPress={() => navigation.navigate('bcone')}
            icon={<GameIcon width={122} height={70} />}
          />
          <View style={styles.separatorView} />
          <Game
            name="Emred"
            time="N/A"
            onPress={() => navigation.navigate('emred')}
            icon={<GameIcon width={122} height={70} />}
          />
        </View>
      </ScrollView>
      {/*</View>*/}
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  indicatorContainer: {
    flex: 1,
    paddingHorizontal: SIZES.gutter,
    backgroundColor: COLORS.grayBg,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    paddingHorizontal: SIZES.gutter,
    backgroundColor: COLORS.grayBg,
  },
  sliderCont: {
    height: 190,
    backgroundColor: COLORS.white,
    marginTop: SIZES.gutter - 5,
    borderRadius: 10,
  },
  balanceCont: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    marginBottom: SIZES.gutter - 10,
  },
  cardBody: {
    flexDirection: 'row',
    marginBottom: SIZES.gutter - 10,
  },
  iconCont: {
    backgroundColor: COLORS.lightBlueBg,
    padding: SIZES.gutter + 2,
    borderRadius: 10,
    marginEnd: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  balanceAmnt: {
    color: COLORS.mainBlue,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.h1,
    fontWeight: '700',
  },
  customBtnStyle: {
    backgroundColor: COLORS.mainBlue,
    paddingVertical: SIZES.gutter - 10,
    width: '50%',
    alignItems: 'center',
    borderRadius: 6,
  },
  separatorTxt: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    fontWeight: '700',
    letterSpacing: 1,
    marginBottom: SIZES.gutter - 10,
  },
  gameCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  separatorView: {
    width: SIZES.gutter - 10,
  },
});

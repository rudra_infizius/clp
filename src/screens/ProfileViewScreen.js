import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, Platform} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Toast from 'react-native-toast-message';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Usericon from '../../assets/images/Usericon.svg';
import Phoneicon from '../../assets/images/phone.svg';
import InputControl from '../components/UI/InputControl';
import Passicon from '../../assets/images/key.svg';
import EyeOpen from '../../assets/images/eyeOpen.svg';
import EyeClose from '../../assets/images/eyeClose.svg';
import ButtonBlock from '../components/UI/ButtonBlock';
import {COLORS, SIZES, FONTS} from '../constants';
import {getInitialUserData, passwordChange} from '../redux/actions';

const Profile = () => {
  const [oldPass, setOldPass] = useState('');
  const [pass, setpass] = useState('');
  const [confirmpass, setconfirmpass] = useState('');
  const [phone, setphone] = useState('');
  const [username, setusername] = useState('');
  const [IsOldPassSecure, setOldPassSecureEntry] = useState(true);
  const [IsNewPassSecure, setNewPassSecureEntry] = useState(true);
  const [IsConfPassSecure, setConfPassSecureEntry] = useState(true);

  const dispatch = useDispatch();
  const {user} = useSelector(state => state.loginReducer);

  const userToken = user.userToken;

  useEffect(() => {
    dispatch(getInitialUserData({token: userToken}));
    // console.log(user);
    setphone(user.userPhone);
    setusername(user.userName);
  }, []);

  const onpassChangeTextHandler = text => {
    setpass(text);
  };
  const onconfirmpassChangeTextHandler = text => {
    setconfirmpass(text);
  };

  const oldPassChangeHandler = text => {
    setOldPass(text);
  };

  const toggleSecureEntryHandler = () => {
    // console.log('pressed');
    setSecureEntry(!secureEntry);
  };

  const updatePasswordHandler = () => {
    if (pass === confirmpass) {
      // console.log(user.userId);
      dispatch(
        passwordChange({
          token: userToken,
          oldPass: oldPass,
          newPass: pass,
          userId: user.userId,
        }),
      ).then(res => {
        if (res.msg === 'Password Changed Successfully!') {
          Toast.show({
            type: 'success',
            text1: res.msg,
          });
          setOldPass('');
          setconfirmpass('');
          setpass('');
        } else {
          Toast.show({
            type: 'error',
            text1: res.msg,
          });
        }
      });
      //8758175871
    } else {
      Toast.show({
        type: 'error',
        text1: "Confirm password doesn't match.",
      });
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.detailsview}>
        <View style={styles.textview}>
          <Text style={styles.text}>Profile Details</Text>
        </View>
        <View>
          <InputControl
            type="square"
            iconBgColor={COLORS.green}
            icon={<Usericon />}
            placeholder=""
            keyboardType="default"
            value={username}
            isSecure={false}
            editable={false}
          />
          <InputControl
            type="square"
            iconBgColor={COLORS.violet}
            icon={<Phoneicon />}
            placeholder=""
            keyboardType="number-pad"
            value={phone}
            isSecure={false}
            editable={false}
          />
        </View>
      </View>
      <View style={styles.detailsview}>
        <View style={styles.textview}>
          <Text style={styles.text}>Change Password</Text>
        </View>
        <View>
          <InputControl
            type="square"
            iconBgColor="#F38D15"
            icon={<Passicon />}
            placeholder="Enter old password"
            keyboardType="default"
            value={oldPass}
            isSecure={IsOldPassSecure}
            onChangeTextHandler={oldPassChangeHandler}
            secondaryItem={
              IsOldPassSecure ? (
                <EyeClose width={20} height={20} />
              ) : (
                <EyeOpen width={20} height={20} />
              )
            }
            secondaryItemAction={() => setOldPassSecureEntry(!IsOldPassSecure)}
          />
          <InputControl
            type="square"
            iconBgColor="#F38D15"
            icon={<Passicon />}
            placeholder="Enter New Password"
            keyboardType="default"
            value={pass}
            isSecure={IsNewPassSecure}
            onChangeTextHandler={onpassChangeTextHandler}
            secondaryItem={
              IsNewPassSecure ? (
                <EyeClose width={20} height={20} />
              ) : (
                <EyeOpen width={20} height={20} />
              )
            }
            secondaryItemAction={() => setNewPassSecureEntry(!IsNewPassSecure)}
          />
          <InputControl
            type="square"
            iconBgColor="#F38D15"
            icon={<Passicon />}
            placeholder="Confirm new Password"
            keyboardType="default"
            value={confirmpass}
            isSecure={IsConfPassSecure}
            onChangeTextHandler={onconfirmpassChangeTextHandler}
            secondaryItem={
              IsConfPassSecure ? (
                <EyeClose width={20} height={20} />
              ) : (
                <EyeOpen width={20} height={20} />
              )
            }
            secondaryItemAction={() =>
              setConfPassSecureEntry(!IsConfPassSecure)
            }
          />
        </View>
      </View>
      <View>
        <ButtonBlock label="Update" onPress={() => updatePasswordHandler()} />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SIZES.gutter,
    flex: 1,
    backgroundColor: COLORS.grayBg,
    paddingTop: SIZES.gutter,
  },
  detailsview: {
    paddingTop: 20,
    paddingHorizontal: 17,
    marginBottom: 20,
    paddingBottom: 9,
    borderRadius: 10,
    backgroundColor: COLORS.white,
  },
  textview: {
    marginBottom: 10,
  },
  text: {
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    letterSpacing: 1,
    color: COLORS.text,
  },
  // updatebtn: {
  //   height: 60,
  //   width: '100%',
  //   backgroundColor: COLORS.mainBlue,
  //   borderRadius: 67,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   position: 'absolute',
  //   bottom: 10,
  //   left: 20,
  // },
});
export default Profile;

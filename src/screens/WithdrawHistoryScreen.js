import React, {useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import RechargeMethodCard from '../components/RechargeMethodCard';
import Paytm from '../../assets/images/Paytm.svg';

import {SIZES, COLORS} from '../constants';
import {getWithdrawalHistory} from '../redux/actions';

const WithdrawHistoryScreen = () => {
  const dispatch = useDispatch();
  const {user, withdrawHistory} = useSelector(state => state.loginReducer);
  const userToken = user.userToken;
  useEffect(() => {
    dispatch(getWithdrawalHistory({token: userToken, pageNumber: '1'}));
    statusHandler();
  }, []);
  // console.log(withdrawHistory.length);
  const status = '';
  const statusHandler = status => {
    if (status === '1') {
      return (status = 'Pending');
    } else if (status === '2') {
      return (status = 'Successfully');
    } else {
      return (status = 'Filed');
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.card}>
        {withdrawHistory.map(item => (
          <RechargeMethodCard
            key={withdrawHistory[item]}
            providerName="Paytm"
            date={item.Date}
            amount={item.Amount}
            status={statusHandler(item.Status)}
            icon={<Paytm width={33} height={45} />}
          />
        ))}
        {/* <RechargeMethodCard
          providerName="Paypal"
          date="07 Jan 2022"
          amount="300"
          status="Successfully"
          icon={<PayPal width={18} height={22} />}
        />
        <RechargeMethodCard
          providerName="Mastercard"
          date="07 Jan 2022"
          amount="300"
          status="Filed"
          icon={<MasterCard width={18} height={22} />}
        /> */}
      </View>

      {/* <View style={styles.btnCont}>
        <ButtonBlock label/>
      </View> */}
    </View>
  );
};

export default WithdrawHistoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.gutter,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
});

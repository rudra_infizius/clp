import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import ButtonBlock from '../components/UI/ButtonBlock';
import {COLORS, SIZES} from '../constants';

import {getLinkedBanks} from '../redux/actions';

const AddbankScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {bank, user} = useSelector(state => state.loginReducer);
  const [state, setstate] = useState({
    bankname: '',
    accountholder: '',
    accountnumber: '',
    accounttype: '',
    bankifsc: '',
    paypassword: '',
  });
  const userToken = user.userToken;
  const banks = bank.totalBankLinked;

  useEffect(() => {
    dispatch(getLinkedBanks({token: userToken}));
  }, []);
  // console.log(userToken);
  function handleChange(evt) {
    const value = evt.target.value;
    setstate({
      ...state,
      [evt.target.name]: value,
    });
  }
  return (
    <View style={{flex: 1, paddingHorizontal: SIZES.gutter}}>
      <View style={styles.card}>
        <View style={styles.textview}>
          <Text style={styles.textstyle}>Your Banks</Text>
        </View>
        {banks.map(item => (
          <TouchableOpacity
            key={item.id}
            style={styles.bankCont}
            onPress={() =>
              navigation.navigate('viewBank', {
                bankId: item.id,
                bankName: item.name,
              })
            }>
            <Text style={styles.textstyle}>{item.name}</Text>
          </TouchableOpacity>
        ))}
        {/* <View style={styles.inputview}>
          <PlainTextinput
            placeholder="Enter Bank Name"
            name="bankname"
            defaultValue={state.bankname}
            onChange={handleChange}
          />
          <PlainTextinput
            placeholder="Enter Account Holder Name"
            name="accountholder"
            defaultValue={state.accountholder}
            onChange={handleChange}
          />
          <PlainTextinput
            placeholder="Enter Bank Account Number"
            name="accountnumber"
            defaultValue={state.accountnumber}
            onChange={handleChange}
          />
          <PlainTextinput
            placeholder="Enter Account Type"
            name="accounttype"
            defaultValue={state.accounttype}
            onChange={handleChange}
          />
          <PlainTextinput
            placeholder="Enter Bank IFSC Code"
            name="bankifsc"
            defaultValue={state.bankifsc}
            onChange={handleChange}
          />
          <PlainTextinput
            placeholder="Enter Pay Password"
            name="paypassword"
            defaultValue={state.paypassword}
            onChange={handleChange}
          />
        </View> */}
      </View>
      <View style={{marginTop: SIZES.gutter}}>
        <ButtonBlock
          label="Add Bank"
          onPress={() => navigation.navigate('addNewBankForm')}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  subview: {
    paddingTop: 15,
    paddingHorizontal: 17,
    marginHorizontal: 10,
    // borderWidth: 1,
    borderRadius: 10,
  },
  textview: {
    marginBottom: 20,
  },
  textstyle: {
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    letterSpacing: 1,
    color: COLORS.text,
  },
  ios_textstyle: {
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    letterSpacing: 1,
    color: COLORS.text,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
  bankCont: {
    padding: SIZES.gutter - 10,
    borderColor: COLORS.grayBg,
    borderWidth: 1,
    borderRadius: 7,
    marginBottom: SIZES.gutter - 10,
  },
  bankName: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
  },
});
export default AddbankScreen;

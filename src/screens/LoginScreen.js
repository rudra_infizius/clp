import React, {useEffect, useState, useRef} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import Toast from 'react-native-toast-message';
import {useNetInfo} from '@react-native-community/netinfo';

import {userLoggedIn} from '../redux/actions';
import {COLORS, SIZES, images, FONTS} from '../constants';
import Phone from '../../assets/images/phone.svg';
import Key from '../../assets/images/key.svg';
import EyeOpen from '../../assets/images/eyeOpen.svg';
import EyeClose from '../../assets/images/eyeClose.svg';
import InputControl from '../components/UI/InputControl';
import ButtonBlock from '../components/UI/ButtonBlock';

const LoginScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const NetInfo = useNetInfo();
  const internetRef = useRef(null);
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [secureEntry, setSecureEntry] = useState(true);

  const {user} = useSelector(state => state.loginReducer);

  const onPhoneChangeTextHandler = text => {
    setPhone(text);
  };

  const onPasswordChangeTextHandler = text => {
    setPassword(text);
  };

  const toggleSecureEntryHandler = () => {
    // console.log('pressed');
    setSecureEntry(!secureEntry);
  };

  const loginHandler = () => {
    // console.log(user.userPhone);
    if (internetRef.current === false) {
      Toast.show({
        type: 'error',
        text1: 'No internet connection.',
        text2: 'Please turn on wifi or mobile data.',
      });
    }
    if (phone.length === 0 || password.length === 0) {
      Toast.show({
        type: 'error',
        text1: 'Please fill out the phone and password.',
      });
    } else {
      dispatch(userLoggedIn({emphone: phone, pass: password})).then(code => {
        if (code == 200) {
          Toast.show({
            type: 'success',
            text1: 'Login successful',
            text2: 'You have successfully loggedIn.',
          });
          navigation.navigate('bottomTabs');
          // console.log(user.userToken);
        } else {
          Toast.show({
            type: 'error',
            text1: 'Unable to Login!!',
            text2: 'Phone number or Password is incorrect.',
          });
        }
      });
    }
  };

  useEffect(() => {
    internetRef.current = NetInfo.isConnected;
    if (internetRef.current === false) {
      Toast.show({
        type: 'error',
        text1: 'No internet connection.',
        text2: 'Please turn on wifi or mobile data.',
      });
    }
  }, [NetInfo.isConnected]);

  return (
    <ImageBackground source={images.backgroundImg} style={styles.container}>
      {/* logo container */}
      <View style={styles.imgCont}>
        <Image source={images.logo} />
      </View>

      {/* screen heading container */}
      <View style={styles.headingCont}>
        <Text style={styles.heading}>Login Now</Text>
        <Text style={styles.helperTxt}>
          Please enter the details below to continue.
        </Text>
      </View>

      {/* login form container */}
      <ScrollView>
        <InputControl
          placeholder="Enter Phone number"
          keyboardType="phone-pad"
          onChangeTextHandler={onPhoneChangeTextHandler}
          value={phone}
          iconBgColor={'#7513F2'}
          icon={<Phone width={15} height={15} />}
          isSecure={false}
          maxLength={10}
        />
        <InputControl
          placeholder="Enter Password"
          keyboardType="default"
          onChangeTextHandler={onPasswordChangeTextHandler}
          value={password}
          iconBgColor={'#F38D15'}
          icon={<Key width={15} height={15} />}
          isSecure={secureEntry}
          secondaryItem={
            secureEntry ? (
              <EyeClose width={17} height={17} />
            ) : (
              <EyeOpen width={17} height={17} />
            )
          }
          secondaryItemAction={toggleSecureEntryHandler}
        />
        {/* forget password container */}
        <View style={styles.frgtPassCont}>
          {/* //TODO: attach forget password btn handler */}
          <TouchableOpacity onPress={() => navigation.navigate('frgtPass')}>
            <Text style={[styles.helperTxt, {fontSize: SIZES.small}]}>
              Forget Password?
            </Text>
          </TouchableOpacity>
        </View>

        {/* login btn */}
        <ButtonBlock label="Login" onPress={loginHandler} />

        {/* register button */}
      </ScrollView>
      <View style={styles.registerBtnCont}>
        <Text style={styles.helperTxt}>Don’t have an account?</Text>
        <TouchableOpacity onPress={() => navigation.navigate('register')}>
          <Text
            style={[
              styles.helperTxt,
              {fontSize: SIZES.body, color: COLORS.mainBlue},
            ]}>
            Register
          </Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: SIZES.gutter,
    // height: Platform.OS === 'ios' ? SIZES.height : null,
  },
  imgCont: {
    alignItems: 'center',
    paddingTop: 10,
    marginBottom: SIZES.gutter - 5,
    marginTop: Platform.OS === 'ios' ? 30 : null,
  },
  headingCont: {
    alignItems: 'center',
    marginBottom: SIZES.gutter + 5,
  },
  heading: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    fontWeight: '700',
    marginBottom: SIZES.gutter - 10,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  frgtPassCont: {
    flexDirection: 'row-reverse',
    marginVertical: SIZES.gutter - 10,
  },
  registerBtnCont: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
  },
});

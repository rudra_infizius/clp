import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Platform,
  SafeAreaView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Clipboard from '@react-native-clipboard/clipboard';
import {useDispatch, useSelector} from 'react-redux';
import Toast from 'react-native-toast-message';

import {images, COLORS, SIZES, FONTS} from '../constants';
import HeaderBar from '../components/HeaderBar';
import AmountDetail from '../components/AmountDetail';
import ButtonBlock from '../components/UI/ButtonBlock';
import ReferralItem from '../components/ReferralItem';
import Money from '../../assets/images/money.svg';
import Chevron from '../../assets/images/chevron.svg';
import UserGreen from '../../assets/images/user_green.svg';
import UserPlus from '../../assets/images/user_plus.svg';
import Copy from '../../assets/images/copy.svg';

import {
  getInitialUserData,
  getWalletAmount,
  getReferralData,
} from '../redux/actions';

const InviteScreen = () => {
  const [refCode, setRefCode] = useState('');
  const [copiedCode, setCodeToClipboard] = useState('');
  const navigation = useNavigation();
  const {user, wallet, referAndEarn} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();

  const userToken = user.userToken;

  // console.log(userToken);

  const initialApiCall = () => {
    dispatch(getInitialUserData({token: userToken}));
    dispatch(getWalletAmount({token: userToken}));
    dispatch(getReferralData({token: userToken})).then(code => {
      setRefCode(code);
    });
  };

  useEffect(() => {
    initialApiCall();
  }, []);

  let totalNotifications = 0;
  let descriptionString = `Get ₹50 Every New Sign-up And Get ₹100
  when your friend activates their account.`;

  const copyToClipboardHandler = code => {
    Clipboard.setString(code);
    Toast.show({
      type: 'success',
      text1: 'Code copied!!',
    });
  };

  return (
    <SafeAreaView>
      {/* header bar */}
      <HeaderBar
        name={user.userName}
        notifications={totalNotifications}
        profileImg={images.profileSmall}
      />
      <ScrollView
        style={styles.container}
        contentContainerStyle={{paddingBottom: SIZES.gutter - 10}}>
        {/* available amount details */}
        <View style={styles.card}>
          <AmountDetail amount={wallet.walletAmount} userID={user.userId} />
          <ButtonBlock
            label="Withdrawal"
            onPress={() => navigation.navigate('withdraw')}
            customStyle={styles.withdrawBtn}
          />
        </View>

        {/* referral information */}

        <ReferralItem
          label="Referral Bonus"
          description={
            referAndEarn.earnedAmount === ''
              ? "You haven't earned any referral bonus yet."
              : referAndEarn.earnedAmount
          }
          icon={<Money width={20} height={20} />}
          iconBgColor="#FFF6E6"
          secondaryItem={<Chevron width={23} height={13} />}
        />
        <ReferralItem
          label="Total Referrals"
          description={referAndEarn.totalReferred}
          iconBgColor="#EAF7F0"
          icon={<UserGreen width={20} height={20} />}
          secondaryItem={<Chevron width={23} height={13} />}
        />
        <ReferralItem
          label="Refer Friends"
          description={descriptionString}
          iconBgColor="#E7F2FE"
          icon={<UserPlus width={20} height={20} />}
          customTitleStyle={styles.titleStyle}
          customDescStyle={styles.descriptionStyle}
        />

        {/* referral code */}
        <View
          style={[
            styles.card,
            {
              flexDirection: 'row',
              alignItems: 'baseline',
              justifyContent: 'space-between',
            },
          ]}>
          <View>
            <Text style={styles.helperTxt}>Your Referral Code</Text>
            <Text style={styles.refCode}>{refCode}</Text>
          </View>
          <TouchableOpacity
            style={styles.copyBtn}
            onPress={() => copyToClipboardHandler(refCode)}>
            <Copy width={17} height={17} />
            <Text style={styles.copyBtnText}>COPY CODE</Text>
          </TouchableOpacity>
        </View>

        {/* redeem code */}
        {/* {refCode !== '' ? (
          <View style={styles.card}>
            <Text style={[styles.titleStyle, {fontSize: SIZES.medium}]}>
              Redeem Code
            </Text>
            <View style={styles.redeemCodeCont}>
              <Text style={styles.refCode}>WR310V</Text>
              <Text style={styles.copyBtnText}>&#x2713; Code Redeemed</Text>
            </View>
          </View>
        ) : null} */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default InviteScreen;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    paddingHorizontal: SIZES.gutter,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
    marginBottom: SIZES.gutter - 10,
  },
  withdrawBtn: {
    backgroundColor: COLORS.green,
    borderRadius: 10,
    paddingVertical: SIZES.gutter - 13,
    alignItems: 'center',
  },
  descriptionStyle: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.tiny + 1,
    textAlign: 'auto',
  },
  titleStyle: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
    textAlign: 'auto',
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  refCode: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.large,
    letterSpacing: 1,
    // marginTop: SIZES.gutter - 15,
  },
  copyBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  copyBtnText: {
    color: COLORS.mainBlue,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
    letterSpacing: 1,
    marginLeft: SIZES.gutter - 15,
  },
  redeemCodeCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 12,
    borderColor: COLORS.mainBlue,
    borderWidth: 1,
    padding: SIZES.gutter - 10,
    marginTop: SIZES.gutter - 10,
  },
});

import React from 'react';
import {ScrollView, SafeAreaView} from 'react-native';
import AccountHeader from '../components/AccountHeader';
import AccountNavBox1 from '../components/AccountNavBox1';
import AccountNavBox2 from '../components/AccountNavBox2';
import {SIZES} from '../constants';
const AccountScreen = () => {
  return (
    <SafeAreaView>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingHorizontal: SIZES.gutter}}>
        <AccountHeader />
        <AccountNavBox1 />
        <AccountNavBox2 />
      </ScrollView>
    </SafeAreaView>
  );
};

export default AccountScreen;

import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Platform,
  SafeAreaView,
} from 'react-native';
import {FlatGrid} from 'react-native-super-grid';
import {useSelector, useDispatch} from 'react-redux';
import Toast from 'react-native-toast-message';

import {images, COLORS, SIZES, FONTS, PAYTM} from '../constants';
import HeaderBar from '../components/HeaderBar';
import InputControl from '../components/UI/InputControl';
import DefaultInput from '../components/UI/DefaultInput';
import AmountDetail from '../components/AmountDetail';
import ButtonBlock from '../components/UI/ButtonBlock';
import RechargeMethodCard from '../components/RechargeMethodCard';
import Rupee from '../../assets/images/rupee.svg';
import CustomModal from '../components/UI/CustomModal';
import Paytm from '../../assets/images/Paytm.svg';

import AllInOneSDKManager from 'paytm_allinone_react-native';
import {generateToken} from '../Payment/Services';

import {
  getWalletAmount,
  getInitialUserData,
  getRechargeHistory,
} from '../redux/actions';
import {useRoute} from '@react-navigation/native';

const RechargeScreen = () => {
  const route = useRoute();
  const orderCounter = useRef(10);
  const [amount, setAmount] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [isDefAmntFocused, setIsDefAmntFocused] = useState(false);

  const {user, wallet, rechargeHistory} = useSelector(
    state => state.loginReducer,
  );
  const dispatch = useDispatch();

  const userToken = user.userToken;

  const modalTypeRef = useRef('');

  const initialApiCall = () => {
    dispatch(getInitialUserData({token: userToken}));
    dispatch(getWalletAmount({token: userToken}));
    dispatch(getRechargeHistory({token: userToken, pageNumber: '1'}));
  };

  useEffect(() => {
    initialApiCall();
  }, []);

  let totalNotifications = 0;
  // TODO: change modal type as per the api response.
  // let modalType = 'success';
  let rechargeAmount = amount;

  const defaultInputOptions = [
    {key: '200', value: '200'},
    {key: '300', value: '300'},
    {key: '500', value: '500'},
    {key: '1000', value: '1000'},
    {key: '2000', value: '2000'},
    {key: '4000', value: '4000'},
  ];
  // converting status from numbers to correct props
  const status = '';
  const statusHandler = status => {
    if (status === '1') {
      return (status = 'Pending');
    } else if (status === '2') {
      return (status = 'Successfully');
    } else {
      return (status = 'Failed');
    }
  };

  const onDefAmntPressHandler = value => {
    setAmount(value);
    setIsDefAmntFocused(!isDefAmntFocused);
    // Toast.show({
    //   type: 'info',
    //   text1: defAmount,
    // });
  };

  const onAmountChangeTextHandler = text => {
    setAmount(text);
  };

  const generateOrderId = prevId => {
    let orderIdInitials = 'ODER-';
    let now = new Date().getUTCMilliseconds();
    let orderId = orderIdInitials.concat(now.toString());
    let counter = prevId + 1;
    return {orderId, counter};
  };

  const PayTmRechargeHandler = async () => {
    let rechargeAmount;
    amount !== '' ? (rechargeAmount = amount) : (rechargeAmount = null);
    // TODO: integrate PayTm payment gateway to recharge the account here.
    console.log(rechargeAmount);

    if (rechargeAmount === '' || rechargeAmount === null) {
      Toast.show({
        type: 'error',
        text1: 'Invalid Amount!',
        text2: 'Enter an amount or select from options.',
      });
      return;
    }

    try {
      // getTxnToken({orderId: 'ODR-001', customerId: '001', txnAmount: amount});
      let response;
      const {orderId, counter} = generateOrderId(orderCounter.current);

      let amt = parseInt(amount);
      let MID = PAYTM.MID;
      let URL_SCHEME = PAYTM.URL_SCHEME;
      let CALLBACK_URL = PAYTM.CALLBACK_URL;

      const token = await generateToken(orderId, amt);

      AllInOneSDKManager.startTransaction(
        orderId,
        MID,
        token,
        amt.toFixed(2),
        CALLBACK_URL + orderId,
        true,
        true,
        URL_SCHEME,
      )
        .then(result => {
          console.log('gateway response', result);
          if (result.RESPCODE === '01') {
            modalTypeRef.current = 'success';
            modalVisibilityHandler();
            orderCounter.current = counter;
          } else {
            modalTypeRef.current = 'fail';
            modalVisibilityHandler();
            orderCounter.current = counter;
          }
        })
        .catch(err => {
          console.log('gateway error', err);
          return err;
        });
      // console.log(response.RESPCODE, 'code....');

      // response = await payNow(amount, orderId);
    } catch (error) {
      console.log(error);
    }
  };

  const modalVisibilityHandler = () => {
    setIsModalVisible(!isModalVisible);
  };
  // TODO: apply try again payment functionality
  const tryAgainPaymentHandler = () => {
    console.log('try again pressed..');
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <CustomModal
        isVisible={isModalVisible}
        type={modalTypeRef.current}
        amount={rechargeAmount}
        modalCloseHandler={modalVisibilityHandler}
        tryAgainHandler={tryAgainPaymentHandler}
      />
      {/* header bar */}
      <HeaderBar
        name={user.userName}
        notifications={totalNotifications}
        profileImg={images.profileSmall}
      />

      <ScrollView
        style={styles.container}
        contentContainerStyle={{paddingBottom: SIZES.gutter - 10}}>
        {/* available amount details */}
        <View style={styles.card}>
          <AmountDetail amount={wallet.walletAmount} userID={user.userId} />
        </View>

        <Text style={styles.separatorTxt}>Select Amount</Text>

        {/* amount selection container */}
        <View style={styles.card}>
          <InputControl
            placeholder="Enter Amount"
            keyboardType="number-pad"
            onChangeTextHandler={onAmountChangeTextHandler}
            value={amount}
            iconBgColor={COLORS.green}
            icon={<Rupee width={9} height={15} />}
            isSecure={false}
            type="square"
            caretHidden={true}
          />
          {/* separator */}
          <View style={styles.amntTypeSeparator}>
            <View style={styles.underline} />
            <Text style={styles.helperTxt}>Or</Text>
            <View style={styles.underline} />
          </View>

          {/* fixed amount selection area */}
          <View>
            <FlatGrid
              data={defaultInputOptions}
              itemDimension={120}
              renderItem={({item}) => (
                <DefaultInput
                  inputKey={item.key}
                  value={item.value}
                  isFocused={isDefAmntFocused}
                  onPress={() => onDefAmntPressHandler(item.value)}
                  stateValue={amount}
                />
              )}
              spacing={SIZES.gutter - 10}
              scrollEnabled={false}
              showsVerticalScrollIndicator={false}
            />
          </View>

          {/* submit button */}
          <ButtonBlock
            label="Recharge"
            onPress={() => PayTmRechargeHandler()}
          />
        </View>

        <Text style={styles.separatorTxt}>Current Recharges</Text>
        {/* transaction details */}
        <View style={styles.card}>
          {rechargeHistory.length === 0 && (
            <Text>You haven't done any recharge yet.</Text>
          )}
          {rechargeHistory.map(item => (
            <RechargeMethodCard
              key={rechargeHistory[item]}
              providerName="Paytm"
              date={item.Time}
              amount={item.Amount}
              status={statusHandler(item.Status)}
              icon={<Paytm width={33} height={45} />}
            />
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default RechargeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flex: Platform.OS === 'android' ? 1 : null,
    paddingHorizontal: SIZES.gutter,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
  },
  separatorTxt: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    fontWeight: '700',
    letterSpacing: 1,
    marginTop: SIZES.gutter,
  },
  amntTypeSeparator: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: SIZES.gutter - 10,
  },
  underline: {
    flex: 1,
    height: 1,
    backgroundColor: COLORS.grayBg,
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  amountRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: SIZES.gutter - 15,
  },
});

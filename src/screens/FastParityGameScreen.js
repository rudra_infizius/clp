import React, {useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  SafeAreaView,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import ButtonToggleGroup from 'react-native-button-toggle-group';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNetInfo} from '@react-native-community/netinfo';
import Modal from 'react-native-modal';
import Toast from 'react-native-toast-message';
import axios from 'axios';

import {COLORS, SIZES, images} from '../constants';
import HeaderBar from '../components/HeaderBar';
import AmountDetail from '../components/AmountDetail';
import ButtonBlock from '../components/UI/ButtonBlock';
import ColorOption from '../components/ColorOption';
import QuestionMark from '../../assets/images/questionMark.svg';
import MoneyButton from '../components/MoneyButton';
import {getInitialUserData, getWalletAmount} from '../redux/actions';
import GameRecordsHeader from '../components/GameRecordsHeader';
import GameRecords from '../components/GameRecords';
import UserRecords from '../components/Records';
import WinnerRecords from '../components/Winnerrecords';
import CountDown from '../components/CountDown';
import BetNumbers from './BetNumbers';
import {BASE_URL} from '../Http/config';

const FastParityGameScreen = () => {
  const [value, setValue] = useState('Game Record');
  const [showBetOptions, setShowBetOptions] = useState(false);
  const [betOption, setbetOption] = useState('');
  const [gameRecords, setGameRecords] = useState([]);
  const [userGameRecords, setUserGameRecords] = useState([]);
  const [winnerRecords, setWinnerRecords] = useState([]);
  const [whichRecordShown, setWhichRecordShown] = useState('game');

  // select state from redux store
  const navigation = useNavigation();
  const {user, wallet} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();

  // hook and ref for netinfo
  const NetInfo = useNetInfo();
  const internetRef = useRef(null);

  // state for placing bet
  const [betAmount, setBetAmount] = useState('');
  const [noOfBets, setNoOfBets] = useState(1);
  const [deliveryAmnt, setDeliveryAmnt] = useState(0);
  const [finalAmount, setFinalAmount] = useState(0);
  const [Fees, setFees] = useState(0);
  // state values for game
  const [period, setPeriod] = useState('');
  const [stopTime, setStopTime] = useState('');
  const [minutesLeft, setMinutesLeft] = useState(null);
  const [secondsLeft, setSecondsLeft] = useState(null);
  const [isTimerOn, setTimerOn] = useState(false);

  const [isBtnDisabled, setIsBtnDisabled] = useState(false);
  // state to handle rendering content and activity indicator
  const [isScreenReady, setIsScreenReady] = useState(false);

  // const userToken = user.userToken;
  const [userToken] = useState(user.userToken);
  // console.log(userToken);
  // first function to get game data
  const getGameData = async ({token}) => {
    const formdata = new FormData();
    formdata.append('token', token);

    try {
      const res = await axios({
        method: 'POST',
        url: BASE_URL + '/color_game.php',
        data: formdata,
      });
      const time = await res.data.response.currentTimer;
      const period = await res.data.response.timerId;

      setPeriod(period);
      // setStopTime(time);
      // setTimerOn(true);
      countDown(time);
    } catch (error) {
      console.log(error);
    }
  };

  // get game data again, when a timer ends.
  const getGameDataSecondTime = ({token, timerId, currentTimer}) => {
    // console.log('token, timerId, currentTimer');
    // console.log(token, timerId, currentTimer);
    const formdata = new FormData();
    formdata.append('token', token);
    formdata.append('currentTimer', currentTimer);
    formdata.append('timerId', timerId);

    axios({
      method: 'POST',
      url: BASE_URL + '/color_game.php',
      data: formdata,
    }).then(res => {
      console.log('second', res.data.response);
      console.log(`second ${res.data.response.currentTimer} `);
      if (res.data.response.timerId !== null) {
        // setStopTime(res.data.response.currentTimer);
        setPeriod(res.data.response.timerId);
        countDown(res.data.response.currentTimer);
      } else {
        console.log('nothing');
      }
    });
  };

  const countDown = time => {
    var resultTime = new Date(time).getTime();
    const timeInterval = setInterval(() => {
      var now = new Date().getTime();
      var duration = resultTime - now;
      // console.log(duration);
      setMinutesLeft(Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60)));
      setSecondsLeft(Math.floor((duration % (1000 * 60)) / 1000));
      // disable the bet btns when 30 secs remaining.
      if (duration <= 30 * 1000) {
        setIsBtnDisabled(true);
        modalCloseHandler();
      } else {
        setIsBtnDisabled(false);
      }
      if (duration < 0 * 1000) {
        clearInterval(timeInterval);
        // getGameDataSecondTime({
        //   token: userToken,
        //   timerId: period,
        //   currentTimer: time,
        // });
      }
    }, 1000);
  };

  const getOtherGameRecords = async ({token}) => {
    const formdata = new FormData();
    formdata.append('token', token);
    try {
      const res1 = await axios({
        method: 'POST',
        url: BASE_URL + '/color_my_bet.php',
        data: formdata,
      });
      const userRecords = await res1.data.response.user_all_bets;
      setUserGameRecords(userRecords);
      // ===============================
      const res2 = await axios({
        method: 'POST',
        url: BASE_URL + '/color_winner.php',
        data: formdata,
      });
      const winnerRecords = await res2.data.response.color_winner;
      setWinnerRecords(winnerRecords);
      // ===============================
      const res3 = await axios({
        method: 'POST',
        url: BASE_URL + '/color_my_bet.php',
        data: formdata,
      });
      const gameData = await res3.data.response.game_results;
      setGameRecords(gameData);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getGameData({token: userToken});
    dispatch(getInitialUserData({token: userToken}));
    dispatch(getWalletAmount({token: userToken}));
    // getting records for first time.
    getOtherGameRecords({token: userToken});

    return () => {
      // setTimerOn(false);
      setGameRecords([]);
      setUserGameRecords([]);
      setWinnerRecords([]);
    };
  }, []);

  useEffect(() => {
    getOtherGameRecords({token: userToken});
    getUserGameRecords({token: userToken});
  }, [period]);

  // calculate the bet amount when a option changes.
  useEffect(() => {
    calculateBetAmount({amount: parseInt(betAmount), noOfTimes: noOfBets});
  }, [betAmount, noOfBets, Fees]);

  let totalNotifications = 0;
  const btnGroupValues = ['Game Record', 'My Records', 'Winners'];

  const selectbetOptionHandler = color => {
    setbetOption(color);
    setShowBetOptions(true);
  };

  const betAmountChangeHandler = value => {
    setBetAmount(value);
  };

  const modalCloseHandler = () => {
    setDeliveryAmnt(0);
    setFinalAmount(0);
    setNoOfBets(1);
    setShowBetOptions(false);
    setBetAmount('');
  };

  const noOfBEtsChangeHandler = value => {
    if (value === '-1') {
      if (noOfBets - 1 < 1) {
        setNoOfBets(1);
      } else {
        setNoOfBets(noOfBets - 1);
      }
    } else if (value === '-5') {
      if (noOfBets - 5 < 1) {
        setNoOfBets(1);
      } else {
        setNoOfBets(noOfBets - 5);
      }
    } else if (value === '+1') {
      if (noOfBets < 1) {
        setNoOfBets(1);
      } else {
        setNoOfBets(noOfBets + 1);
      }
    } else if (value === '+5') {
      if (noOfBets < 1) {
        setNoOfBets(1);
      } else {
        setNoOfBets(noOfBets + 5);
      }
    }
  };

  const calculateBetAmount = ({amount, noOfTimes}) => {
    // noOfTimes is a value set in modal by "noOfBEtsChangeHandler".
    let initialDeliveryAmnt = amount * noOfTimes;
    // 2% fees
    setFees(initialDeliveryAmnt * 0.02);
    let finalDeliveryAmnt = initialDeliveryAmnt - Fees;

    setDeliveryAmnt(parseFloat(finalDeliveryAmnt));
    setFinalAmount(parseFloat(finalDeliveryAmnt));
  };

  // buttontogglegroup change handler
  const buttonToggleHandler = val => {
    setValue(val);
    // console.log(value);
    if (val === 'Game Record') {
      setWhichRecordShown('game');
    }
    if (val === 'My Records') {
      setWhichRecordShown('my');
    }
    if (val === 'Winners') {
      setWhichRecordShown('winners');
    }
  };

  const getUserGameRecords = async ({token}) => {
    const formdata = new FormData();
    formdata.append('token', token);

    const res1 = await axios({
      method: 'POST',
      url: BASE_URL + '/color_my_bet.php',
      data: formdata,
    });
    const userRecords = await res1.data.response.user_all_bets;
    setUserGameRecords(userRecords);
  };

  // place my bet handler
  const placeBetHandler = ({
    token,
    color,
    userId,
    timerId,
    optRadio,
    multiples,
  }) => {
    const formData = new FormData();
    formData.append('token', token);
    formData.append('submit', 'submit');
    formData.append('color', color);
    formData.append('userid', userId);
    formData.append('timerid', timerId);
    formData.append('optradio', optRadio);
    formData.append('multiples', multiples);

    axios({
      method: 'POST',
      url: BASE_URL + '/includes/colorspot.php',
      data: formData,
    }).then(res => {
      console.log(res);
      setShowBetOptions(false);
      if (res.data.response.msg.includes('Sorry')) {
        Toast.show({
          type: 'error',
          text1: 'Sorry your balance is too low.',
        });
      } else if (res.data.response.redirect != '') {
        Toast.show({
          type: 'success',
          text1: 'Your bet has been placed successful.',
        });
        dispatch(getWalletAmount({token: userToken}));
        getUserGameRecords({token: userToken});
      }
    });
  };

  // effect for listening internet conn
  // useEffect(() => {
  //   internetRef.current = NetInfo.isConnected;
  //   if (internetRef.current === false) {
  //     Toast.show({
  //       type: 'error',
  //       text1: 'No internet connection.',
  //       text2: 'Please turn on wifi or mobile data.',
  //     });
  //   }
  // }, [NetInfo.isConnected]);

  // if (internetRef.current === false) {
  //   return (
  //     <ActivityIndicator
  //       color={COLORS.mainBlue}
  //       size={'large'}
  //       style={styles.indicatorContainer}
  //     />
  //   );
  // }

  if (period == '') {
    return (
      <ActivityIndicator
        color={COLORS.mainBlue}
        size={'large'}
        style={styles.indicatorContainer}
      />
    );
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* header bar */}
      <HeaderBar
        name={user.userName}
        notifications={totalNotifications}
        profileImg={images.profileSmall}
      />

      <ScrollView
        style={styles.container}
        contentContainerStyle={{paddingBottom: SIZES.gutter - 10}}>
        {/* available amount details */}
        <View style={styles.card}>
          <AmountDetail amount={wallet.walletAmount} userID={user.userId} />
          <ButtonBlock
            label="Withdrawal"
            onPress={() => navigation.navigate('withdraw')}
            customStyle={styles.withdrawBtn}
          />
        </View>
        {/* game information */}
        <View
          style={[
            styles.card,
            {
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            },
          ]}>
          <View>
            <Text style={styles.helperTxt}>Period</Text>
            <Text style={[styles.bigText, {color: COLORS.text}]}>{period}</Text>
          </View>
          <CountDown minutes={minutesLeft} seconds={secondsLeft} />
        </View>
        {/* colors available */}
        <View
          style={[
            styles.card,
            {
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-evenly',
            },
          ]}>
          <ColorOption
            label="Green"
            bgColor={COLORS.greenBg}
            txtColor={COLORS.green}
            onPress={() => selectbetOptionHandler('green')}
            disabled={isBtnDisabled}
          />
          <ColorOption
            label="Violet"
            bgColor={COLORS.violetBg}
            txtColor={COLORS.violet}
            onPress={() => selectbetOptionHandler('violet')}
            disabled={isBtnDisabled}
          />
          <ColorOption
            label="Red"
            bgColor={COLORS.redBg}
            txtColor={COLORS.red}
            onPress={() => selectbetOptionHandler('red')}
            disabled={isBtnDisabled}
          />
        </View>
        {/* modal for confirming the bet, showed when a user select a color or number */}
        {showBetOptions && (
          <Modal
            isVisible={showBetOptions}
            animationIn="bounceInUp"
            animationInTiming={900}
            animationOut="bounceOutDown"
            animationOutTiming={900}
            backdropColor={COLORS.black}
            onBackdropPress={() => {}}
            onBackButtonPress={() => modalCloseHandler()}
            style={styles.betOptionCont}>
            <View
              style={[
                styles.optionHeader,
                // dynamic header background color
                betOption === 'green'
                  ? {backgroundColor: COLORS.green}
                  : betOption === 'violet'
                  ? {backgroundColor: COLORS.violet}
                  : betOption === 'red'
                  ? {backgroundColor: COLORS.red}
                  : {backgroundColor: COLORS.mainBlue},
              ]}>
              <Text style={styles.headerTxt}>Join {betOption}</Text>
              <TouchableOpacity onPress={() => modalCloseHandler()}>
                <Icon name="close" color={COLORS.white} size={23} />
              </TouchableOpacity>
            </View>
            <View style={styles.betMoneyOptions}>
              <Text>Contract</Text>
              <View style={styles.moneySelectionBtnCont}>
                <MoneyButton
                  value={betAmount}
                  label="10"
                  onPress={() => betAmountChangeHandler('10')}
                />
                <MoneyButton
                  value={betAmount}
                  label="100"
                  onPress={() => betAmountChangeHandler('100')}
                />
                <MoneyButton
                  value={betAmount}
                  label="1000"
                  onPress={() => betAmountChangeHandler('1000')}
                />
                <MoneyButton
                  value={betAmount}
                  label="10000"
                  onPress={() => betAmountChangeHandler('10000')}
                />
              </View>
            </View>
            <View style={styles.betNumberOptions}>
              <Text>Number</Text>
              <View style={styles.moneySelectionBtnCont}>
                <TouchableOpacity
                  style={styles.moneyBtn}
                  onPress={() => noOfBEtsChangeHandler('-5')}>
                  <Text style={styles.helperTxt}>- 5</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.moneyBtn}
                  onPress={() => noOfBEtsChangeHandler('-1')}>
                  <Text style={styles.helperTxt}>- 1</Text>
                </TouchableOpacity>
                <View>
                  <Text style={styles.helperTxt}>{noOfBets}</Text>
                </View>
                <TouchableOpacity
                  style={styles.moneyBtn}
                  onPress={() => noOfBEtsChangeHandler('+1')}>
                  <Text style={styles.helperTxt}>+ 1</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.moneyBtn}
                  onPress={() => noOfBEtsChangeHandler('+5')}>
                  <Text style={styles.helperTxt}>+ 5</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  alignSelf: 'flex-end',
                  marginTop: 15,
                  paddingRight: 15,
                }}>
                <Text style={styles.helperTxt}>Delivery: {deliveryAmnt}</Text>
                <Text style={styles.helperTxt}>
                  Fee: {Fees == NaN ? 0 : Fees}
                </Text>
                <Text style={styles.helperTxt}>Amount: {finalAmount}</Text>
              </View>
            </View>
            <View style={styles.optionFooter}>
              <TouchableOpacity
                style={styles.modalBtn}
                onPress={() => modalCloseHandler()}>
                <Text style={[styles.descText, {color: COLORS.black}]}>
                  Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  placeBetHandler({
                    token: userToken,
                    color: betOption,
                    userId: user.userId,
                    optRadio: betAmount,
                    multiples: noOfBets,
                    timerId: period,
                  })
                }
                style={[
                  styles.modalBtn,
                  {
                    backgroundColor: betOption,
                  },
                ]}>
                <Text style={[styles.descText]}>Confirm</Text>
              </TouchableOpacity>
            </View>
          </Modal>
        )}
        {/* modal ends here */}
        {/* records button group */}
        <ButtonToggleGroup
          highlightBackgroundColor={COLORS.mainBlue}
          highlightTextColor={COLORS.white}
          inactiveBackgroundColor={COLORS.white}
          inactiveTextColor={styles.descText}
          values={btnGroupValues}
          value={value}
          onSelect={val => buttonToggleHandler(val)}
          style={styles.btnGroupStyle}
          textStyle={styles.btnGroupTxtStyle}
        />
        {/* records table */}
        <View style={styles.contentDesc}>
          <Text style={styles.descText}>Parity Records</Text>
          <View style={styles.contentDesc}>
            <QuestionMark width={16} height={16} />
            <Text style={[styles.helperTxt, {marginLeft: 5}]}>Rules</Text>
          </View>
        </View>
        {/* records */}
        {whichRecordShown === 'game' && <GameRecordsHeader />}
        {whichRecordShown === 'game' ? (
          gameRecords.length > 0 ? (
            gameRecords.map(item => (
              // console.log(item.Period),
              <GameRecords
                key={gameRecords[item]}
                period={item.Period}
                color={item.Color}
                price={item.Price}
              />
            ))
          ) : (
            <Text style={styles.helperTxt}>No records</Text>
          )
        ) : whichRecordShown === 'my' ? (
          userGameRecords != null ? (
            userGameRecords.map(item => (
              <UserRecords
                key={userGameRecords[item]}
                amount={item.Amount}
                date={item.Time}
                period={item.Period}
                status={item.Status}
                result={item.Result}
                color={item.Select}
                prepay={item['Pre Pay']}
                fee={item.Fee}
                win={item.Win}
              />
            ))
          ) : (
            <Text style={styles.helperTxt}>No records</Text>
          )
        ) : whichRecordShown === 'winners' ? (
          winnerRecords != null ? (
            winnerRecords.map(item => (
              <WinnerRecords
                key={winnerRecords[item]}
                amount={item.win_amount}
                name={item.User}
                period={item.Period}
                color={item.color}
              />
            ))
          ) : (
            <Text style={styles.helperTxt}>No records</Text>
          )
        ) : (
          <View />
        )}
        {/* {recordShowhandler()} */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default FastParityGameScreen;

const styles = StyleSheet.create({
  indicatorContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    paddingHorizontal: SIZES.gutter,
  },
  card: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 5,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
    marginBottom: SIZES.gutter - 10,
  },
  withdrawBtn: {
    backgroundColor: COLORS.green,
    borderRadius: 10,
    paddingVertical: SIZES.gutter - 13,
    alignItems: 'center',
  },
  helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  ios_helperTxt: {
    color: COLORS.defaultTxt,
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.body,
    fontWeight: '400',
  },
  bigText: {
    color: COLORS.mainBlue,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    letterSpacing: 1,
    marginLeft: SIZES.gutter - 15,
  },
  ios_bigText: {
    color: COLORS.mainBlue,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.headingsLabel,
    letterSpacing: 1,
    marginLeft: SIZES.gutter - 15,
  },
  btnGroupStyle: {
    marginTop: SIZES.gutter,
    padding: SIZES.gutter - 15,
    borderRadius: 8,
    borderWidth: 1,
    backgroundColor: COLORS.white,
    borderColor: COLORS.white,
  },
  btnGroupTxtStyle: {
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.tiny + 1,
  },
  ios_btnGroupTxtStyle: {
    fontFamily: 'Biryani-Regular',
    fontSize: SIZES.tiny + 1,
  },
  contentDesc: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: SIZES.gutter - 15,
  },
  descText: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
  },
  ios_descText: {
    color: COLORS.text,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.body,
  },
  betOptionCont: {
    backgroundColor: COLORS.white,
    borderRadius: 10,
    maxHeight: '50%',
    top: 100,
    // alignItems: 'center',
  },
  optionHeader: {
    backgroundColor: COLORS.green,
    padding: SIZES.gutter - 10,
    position: 'absolute',
    top: 0,
    width: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerTxt: {
    color: COLORS.white,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    letterSpacing: 1,
  },
  ios_headerTxt: {
    color: COLORS.white,
    fontFamily: 'Biryani-Bold',
    fontSize: SIZES.medium,
    letterSpacing: 1,
  },
  betMoneyOptions: {
    position: 'absolute',
    top: 47,
    padding: SIZES.gutter - 10,
    width: '100%',
  },
  moneySelectionBtnCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginTop: SIZES.gutter - 10,
  },
  moneyBtn: {
    borderRadius: 7,
    borderColor: COLORS.defaultTxt,
    borderWidth: 1,
    padding: SIZES.gutter - 10,
  },
  betNumberOptions: {
    padding: SIZES.gutter - 10,
    width: '100%',
    marginTop: SIZES.gutter,
    position: 'absolute',
    top: 120,
    // backgroundColor: COLORS.redBg,
  },
  optionFooter: {
    padding: SIZES.gutter - 10,
    width: '100%',
    marginTop: SIZES.gutter,
    position: 'absolute',
    top: 300,
    // backgroundColor: COLORS.lightBlueBg,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  shadow: {
    shadowColor: '#171717',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  modalBtn: {
    paddingVertical: 7,
    paddingHorizontal: SIZES.gutter,
    borderRadius: 5,
    backgroundColor: COLORS.grayBg,
  },
});

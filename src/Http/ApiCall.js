import axios from 'axios';
import {BASE_URL} from './config';

//   var formdata = new FormData();
//   var myHeaders = new Headers();
//   formdata.append('token', 'd7f44c6a24ea83075fdb59ff2000deeb');
//   myHeaders.append('Cookie', 'PHPSESSID=48jfpj40n8pd4opoi2hem4ino6');
//   formdata.append('emphone', '8107224909');
//   formdata.append('pass', 'shub@123');
//   formdata.append('submit', 'submit');
//   formdata.append('phone', '8107224909');
//   formdata.append('pass', 'shub@123');
//   formdata.append('referalCode', 'dsasd');

//   formdata.append('uname', 'kartik');
//   formdata.append('phone', '1234567980');
//   formdata.append('pass', 'shub@123');
//   formdata.append('refcode', '');

//   formdata.append('otphtml', '546789');
//   formdata.append('uname_p', 'shubham');
//   formdata.append('phone_p', '8107224909');
//   formdata.append('pass_p', 'shub@123');
//   formdata.append('ref_p', 'asdasd');

let baseUrl = BASE_URL;

export const profile_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/profile.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.available_balance);
      console.log(res.data.response.token);
    })
    .catch(err => console.log(err));
};

export const View_my_profile_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/my_profile.php',
    data: formdata, //'token'
    // body: formdata,
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.user_data.userid);
      console.log(res.data.response.user_data.username);
      console.log(res.data.response.user_data.phone);
      console.log(res.data.response.token);
    })
    .catch(err => console.log(err));
};

export const update_my_profile_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/my_profile.php',
    data: formdata, //token,name,submit
  })
    .then(res => {
      console.log(res);
    })
    .catch(err => console.log(err));
};

export const login_post = async () => {
  const formdata = new FormData();
  formdata.append('emphone', '8107224909');
  formdata.append('pass', 'shub@123');
  formdata.append('submit', 'submit');
  try {
    const res = await axios({
      method: 'POST',
      url: baseUrl + '/login.php',
      data: formdata, //'emphone','pass','submit
      // body: formdata,
    });
    if (res.data) {
      console.log(res.data);
    }
  } catch (error) {
    console.log(error);
  }
  // .then(res => {
  //   console.log(res);
  //   console.log(res.data.response.msg);
  //   console.log(res.data.response.data);
  // })
  // .catch(err => console.log(err));
};
export const gettoken_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/get_token.php',
    data: formdata, //'phone','pass'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.token);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const index_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/index.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.marque_text);
      console.log(res.data.response.image);
    })
    .catch(err => console.log(err));
};
export const signup_get = () => {
  axios({
    method: 'GET',
    url: baseUrl + '/sign-up.php',
    data: formdata, //'referalCode'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response);
    })
    .catch(err => console.log(err));
};

export const otp_signup_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/otp-sign-up.php',
    data: formdata, //'uname','phone','pass','refcode'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const otp_signup_resend_otp_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/otp-sign-up.php',
    data: formdata, //'uname','phone','resend','refcode'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const verify_otp_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/verify.php',
    data: formdata, //'otphtml','uname_p','phone_p','pass_p','ref_p'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const topup_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/top-up.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
      console.log(res.data.response.wallet);
    })
    .catch(err => console.log(err));
};

export const referandearn_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/refer.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const view_change_pass_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/change-pass.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const update_change_pass_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/change-pass.php?type',
    data: formdata, //type,userid,oldpass,pass,submit,'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const my_wallet_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/my_wallet.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const withdraw_form_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/withdraw.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const withdraw_money_request_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/withdraw.php',
    data: formdata, //'token',withAm,bankcard,wpass,submit
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const add_bank_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/add-new-bank.php',
    data: formdata, //'token',withAm,bankcard,wpass,submit
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const addbank_submitdata_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/includes/add-bank.php',
    data: formdata, //type,userid,bankname,acc_num,ifsc_code,pay_pass,submit,'token',acc_type,acc_name
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const view_my_bank = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/my_bank.php',
    data: formdata, //'token'
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const view_all_game_transaction_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/all-transactions.php',
    data: formdata, //token
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const view_recharge_record = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/record-recharge.php',
    data: formdata, //token
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const withdrawal_records_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/record-withdraw.php',
    data: formdata, //token,page_number
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const get_token_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/get_token.php',
    data: formdata, //phone,pass
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const termsandcondition_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/terms-and-conditions.php',
    data: formdata, //token
  })
    .then(res => {
      console.log(res);
    })
    .catch(err => console.log(err));
};

export const privacy_policy_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/privacy-policy.php',
    data: formdata, //token
  })
    .then(res => {
      console.log(res);
    })
    .catch(err => console.log(err));
};

export const logout_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/logout.php',
    data: formdata, //token
  })
    .then(res => {
      console.log(res);
    })
    .catch(err => console.log(err));
};

export const forget_login_pass_form_get = () => {
  axios({
    method: 'GET',
    url: baseUrl + '/forgot-password.php',
    headers: {
      myHeaders, //Cookie
    },
  })
    .then(res => {
      console.log(res);
    })
    .catch(err => console.log(err));
};

export const reset_password_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/reset-password.php',
    data: formdata, //phone,submit
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const reset_verify_otp_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/reset-verify.php',
    data: formdata, //phone,otp,pass,submit
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const view_bank_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/view_bank.php',
    data: formdata, //token,id
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const modify_bank_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/includes/add-bank.php',
    data: formdata, //type,userid,bank_name,acc_name,ifsc_code,id,submit,token,acc_type,acc_name
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const my_bet_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/color_my_bet.php',
    data: formdata, //token,pagenumber
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const color_game_firsttime_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/color_game.php',
    data: formdata, //token
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const color_game_second_time_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/color_game.php',
    data: formdata, //token
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const color_spot_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + 'colorspot.php',
    data: formdata, //token,submit,color,userid,timerid,optradio,multiples
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const color_winners_record_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/color_winner.php',
    data: formdata, //token
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

export const color_game_rules_post = () => {
  axios({
    method: 'POST',
    url: baseUrl + '/color_game_rules.php',
    data: formdata, //token
    headers: myHeaders, //Cookie
  })
    .then(res => {
      console.log(res);
      console.log(res.data.response.msg);
    })
    .catch(err => console.log(err));
};

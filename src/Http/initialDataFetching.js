import {useSelector, useDispatch} from 'react-redux';
import {getInitialUserData, getWalletAmount} from '../redux/actions';

export const initialDataFetching = () => {
  const {user} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();

  const userToken = user.userToken;

  dispatch(getInitialUserData({token: userToken}));
  dispatch(getWalletAmount({token: userToken}));
};
